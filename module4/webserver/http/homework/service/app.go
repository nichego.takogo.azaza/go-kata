package service

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-chi/chi/v5"
	log "github.com/sirupsen/logrus"
	hndl "gitlab.com/nichego.takogo.azaza/go-kata/module4/webserver/http/homework/handlers"
)

func Run() {
	log.New().Infoln("Func: Run")
	m := chi.NewMux()
	m.Get("/", hndl.Hello)
	m.Route("/users", func(r chi.Router) {
		r.Get("/", hndl.UsersGet)
		r.Get("/{id}", hndl.UserGet)
		r.Post("/", hndl.UserPost)
	})
	m.Post("/upload", hndl.Upload)
	m.Route("/public", func(r chi.Router) {
		r.Get("/", hndl.Public)
		r.Get("/{filename}", hndl.PublicFile)
	})
	port := ":8080"
	srv := &http.Server{
		Addr:    port,
		Handler: m,
	}
	go func() {
		log.Infoln("server started on port", port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Errorln("listen:", err)
		}
	}()
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Infoln("Shutdown Server ...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Errorln("Server Shutdown:", err)
	}
	log.Infoln("Server exiting")
}
