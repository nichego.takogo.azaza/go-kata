package handlers

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"
	log "github.com/sirupsen/logrus"
	rep "gitlab.com/nichego.takogo.azaza/go-kata/module4/webserver/http/homework/repository"
)

func js(a interface{}, w http.ResponseWriter) string {
	log.New().Infoln("Func: js")
	js, err := json.Marshal(a)
	if err != nil {
		log.New().Errorln("Cant Marshal")
		fmt.Fprint(w, err)
	}
	return string(js)
}
func Hello(w http.ResponseWriter, r *http.Request) {
	log.New().Info("Func: Hello")
	fmt.Fprint(w, js("Приветственное сообщение", w))
}
func UsersGet(w http.ResponseWriter, r *http.Request) {
	log.New().Infoln("Func: UsersGet")
	fmt.Fprint(w, js(rep.Users, w))
}
func UserGet(w http.ResponseWriter, r *http.Request) {
	log.New().Infoln("Func: UserGet")
	id := chi.URLParam(r, "id")
	n, err := strconv.Atoi(id)
	if err != nil {
		log.New().Errorln("Cant strconv.Atoi(id)")
		fmt.Fprint(w, "Wrong URL")
		return
	}
	if n < 0 || n >= len(rep.Users) {
		log.New().Errorln("Invalid values")
		fmt.Fprintf(w, "There is no id:%d", n)
	} else {
		fmt.Fprint(w, js(rep.Users[n], w))
	}
}
func UserPost(w http.ResponseWriter, r *http.Request) {
	log.New().Infoln("Func: UserPost")
	b := make([]byte, 16)
	res := make([]byte, 0, 64)
	var err error
	n := 0
	for err == nil {
		n, err = r.Body.Read(b)
		res = append(res, b[:n]...)
	}
	var user rep.User
	err = json.Unmarshal(res, &user)
	if user.Name == "" || user.Age < 18 {
		log.New().Errorln("Invalid values")
		err = fmt.Errorf("\"Name\" cant be empty, and \"Age\" must be over 18. Example: {\"Name\":\"Alex\",\"Age\":18}")
	}
	if err == nil {
		user.ID = len(rep.Users)
		rep.Append(user)
		fmt.Fprint(w, "Succes!")
	} else {
		fmt.Fprint(w, "Fail!", err)
	}
}
func Upload(w http.ResponseWriter, r *http.Request) {
	log.New().Infoln("Func: Upload")
	file, fileHeader, err := r.FormFile("uploadfile")
	if err != nil {
		log.New().Errorln("Cant get file")
		fmt.Fprint(w, err)
		return
	}
	file.Close()
	os.Mkdir(rep.UploadPath, os.ModeDir)
	var fileName, leftPart, rightPart string
	slc := strings.Split(fileHeader.Filename, ".")
	rightPart = "." + slc[len(slc)-1]
	for i := 0; i < len(slc)-1; i++ {
		if i > 0 {
			leftPart += "."
		}
		leftPart += slc[i]
	}
	for i := 0; err == nil; i++ {
		if i == 0 {
			fileName = leftPart + rightPart
		} else {
			fileName = leftPart + fmt.Sprint("_", i) + rightPart
		}
		_, err = os.Open(rep.UploadPath + fileName)
	}
	servFile, err := os.OpenFile(rep.UploadPath+fileName, os.O_WRONLY|os.O_CREATE, 0)
	if err != nil {
		log.New().Errorln("Cant open/create file")
		fmt.Fprint(w, err)
		return
	}
	defer servFile.Close()
	_, err = io.Copy(servFile, file)
	if err != nil {
		log.New().Errorln("Cant io.Copy")
		fmt.Fprint(w, err)
	}
}
func Public(w http.ResponseWriter, r *http.Request) {
	log.New().Infoln("Func: Public")
	arr, err := os.ReadDir(rep.UploadPath)
	if err != nil {
		log.New().Errorf("Cant find dir \"%s\"", rep.UploadPath)
		fmt.Fprint(w, err)
		return
	}
	fmt.Fprintln(w, "Files:")
	for i := range arr {
		fmt.Fprintln(w, arr[i].Name())
	}
}
func PublicFile(w http.ResponseWriter, r *http.Request) {
	log.New().Infoln("Func: PublicFile")
	filename := chi.URLParam(r, "filename")
	file, err := os.OpenFile(rep.UploadPath+filename, os.O_RDONLY, 0)
	if err != nil {
		log.New().Errorln("Cant open file")
		fmt.Fprint(w, err)
		return
	}
	b := make([]byte, 32)
	for {
		n, err := file.Read(b)
		if err != nil {
			break
		}
		w.Write(b[:n])
	}
}
