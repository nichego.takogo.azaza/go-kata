package main

import (
	srvc "gitlab.com/nichego.takogo.azaza/go-kata/module4/webserver/http/homework/service"
)

func main() {
	srvc.Run()
}
