package repository

func init() {
	Users = append(Users, User{0, "Alex", 25})
	Users = append(Users, User{1, "Mike", 31})
	Users = append(Users, User{2, "John", 17})
}

var UploadPath = "public/"

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Age  int    `json:"age"`
}

var Users = make([]User, 0, 16)

func Append(u User) {
	Users = append(Users, u)
}
