package swaggercode

import (
	mdl "gitlab.com/nichego.takogo.azaza/go-kata/module4/webserver/swagger/models"
)

//go:generate swagger generate spec -o public/swagger.json --scan-models

// swagger:route POST /pet/{petId}/uploadImage pet petImgRequest
// uploads an image.
// responses:
//   200:petImgResponse

// swagger:parameters petImgRequest
type petImgRequest struct {
	// ID of pet to update
	// in:path
	// required:true
	PetID int64 `json:"petId"`
	// Additional data to pass to server
	// in:formData
	AdditionalMetadata string `json:"additionalMetadata"`
	// file to upload
	// in: formData
	// swagger:file
	File string `json:"file"`
}

// swagger:response petImgResponse
type petImgResponse struct {
	// in:body
	Body mdl.ApiResponse
}

// swagger:route POST /pet pet petRequest
// Add a new pet to the store.
// responses:
//   405:petResponse

// swagger:parameters petRequest
type petRequest struct {
	// Pet object that needs to be added to the store
	// in:body
	// required:true
	Body mdl.Pet `json:"body"`
}

// swagger:response petResponse
type petResponse struct {
}

// swagger:route PUT /pet pet petUpdRequest
// Update an existing pet.
// responses:
//   400:petUpdResponse
//	 404:petUpdResponse
//	 405:petUpdResponse

// swagger:parameters petUpdRequest
type petUpdRequest struct {
	// Pet object that needs to be updated
	// in:body
	// required:true
	Body mdl.Pet `json:"body"`
}

// swagger:response petUpdResponse
type petUpdResponse struct {
}

// swagger:route GET /pet/findByStatus pet petFindRequest
// Finds pets by status.
// Multiple status values can be provided with comma separated strings
// responses:
//   200:petFind200Response
//   400:petFind400Response

// swagger:parameters petFindRequest
type petFIndRequest struct {
	// Status values that need to be considered for filter
	// in:query
	// enum: available,pending,sold
	// required:true
	Status string `json:"status"`
}

// swagger:response petFind200Response
type petFInd200Response struct {
	// in:body
	Body []mdl.Pet
}

// swagger:response petFind400Response
type petFInd400Response struct {
}

// swagger:route GET /pet/{petId} pet petIDRequest
// Find pet by ID.
// Returns a single pet
// responses:
//   200:petID200Response
//   400:petID400Response
//   404:petID400Response

// swagger:parameters petIDRequest
type petIDRequest struct {
	// ID of pet to return
	// in:path
	// required:true
	PetID int64 `json:"petId"`
}

// swagger:response petID200Response
type petID200Response struct {
	// in:body
	Body mdl.Pet
}

// swagger:response petID400Response
type petID400Response struct {
}

// swagger:route POST /pet/{petId} pet petIDPRequest
// Updates a pet in the store with form data.
// responses:
//   405:petIDPResponse

// swagger:parameters petIDPRequest
type petIDPRequest struct {
	// ID of pet that needs to be updated
	// in:path
	// required:true
	PetID int64 `json:"petId"`
	// Updated name of the pet
	// in:formData
	Name string `json:"name"`
	// Updated status of the pet
	// in:formData
	Status string `json:"status"`
}

// swagger:response petIDPResponse
type petIDPResponse struct {
}

// swagger:route DELETE /pet/{petId} pet petDelRequest
// Deletes a pet.
// responses:
//   400:petDelResponse
//   404:petDelResponse

// swagger:parameters petDelRequest
type petDelRequest struct {
	// in:header
	ApiKey string `json:"api_key"`
	// Pet ID to delete
	// in:path
	// required:true
	PetID int64 `json:"petId"`
}

// swagger:response petDelResponse
type petDelResponse struct {
}

// swagger:route POST /store/order store storeOrdRequest
// Place an order for a pet.
// responses:
//   200:storeOrd200Response
//   400:storeOrd400Response

// swagger:parameters storeOrdRequest
type storeOrdRequest struct {
	// order placed for purchasing the pet
	// in:body
	// required:true
	Body mdl.Order `json:"body"`
}

// swagger:response storeOrd200Response
type storeOrd200Response struct {
	// in:body
	Body mdl.Order
}

// swagger:response storeOrd400Response
type storeOrd400Response struct {
}

// swagger:route GET /store/order/{orderId} store storeIDRequest
// Find purchase order by ID.
// For valid response try integer IDs with value >= 1 and <= 10. Other values will generated exceptions
// responses:
//   200:storeID200Response
//   400:storeID400Response
//   404:storeID400Response

// swagger:parameters storeIDRequest
type storeIDRequest struct {
	// ID of pet that needs to be fetched
	// in:path
	// required:true
	OrderID int64 `json:"orderId"`
}

// swagger:response storeID200Response
type storeID200Response struct {
	// in:body
	Body mdl.Order
}

// swagger:response storeID400Response
type storeID400Response struct {
}

// swagger:route DELETE /store/order/{orderId} store storeDelRequest
// Delete purchase order by ID.
// For valid response try integer IDs with positive integer value. Negative or non-integer values will generate API errors
// responses:
//   400:storeDelResponse
//   404:storeDelResponse

// swagger:parameters storeDelRequest
type storeDelRequest struct {
	// ID of the order that needs to be deleted
	// in:path
	// required:true
	OrderID int64 `json:"orderId"`
}

// swagger:response storeDelResponse
type storeDelResponse struct {
}

// swagger:route GET /store/inventory store storeInvRequest
// Returns pet inventories by status.
// Returns a map of status codes to quantities
// responses:
//   200:storeInvResponse

// swagger:parameters storeInvRequest
type storeInvRequest struct {
}

// swagger:response storeInvResponse
type storeInvResponse struct {
	// in:body
	Map map[string]int64
}

// swagger:route POST /user/createWithArray user userArrRequest
// Creates list of users with given input array.
// responses:
//   default:userArrResponse

// swagger:parameters userArrRequest
type userArrRequest struct {
	// List of user object
	// in:body
	// required:true
	Body []mdl.User `json:"body"`
}

// swagger:response userArrResponse
type userArrResponse struct {
}

// swagger:route POST /user/createWithList user userLstRequest
// Creates list of users with given input array.
// responses:
//   default:userLstResponse

// swagger:parameters userLstRequest
type userLstRequest struct {
	// List of user object
	// in:body
	// required:true
	Body []mdl.User `json:"body"`
}

// swagger:response userLstResponse
type userLstResponse struct {
}

// swagger:route GET /user/{username} user userGetRequest
// Get user by user name.
// responses:
//   200:userGet200Response
//   400:userGet400Response
//   404:userGet400Response

// swagger:parameters userGetRequest
type userGetRequest struct {
	// The name that needs to be fetched. Use user1 for testing.
	// in:path
	// required:true
	Username string `json:"username"`
}

// swagger:response userGet200Response
type userGet200Response struct {
	// in:body
	Body mdl.User
}

// swagger:response userGet400Response
type userGet400Response struct {
}

// swagger:route PUT /user/{username} user userPutRequest
// Update user.
// This can only be done by the logged in user.
// responses:
//   400:userPutResponse
//   404:userPutResponse

// swagger:parameters userPutRequest
type userPutRequest struct {
	// name that need to be updated
	// in:path
	// required:true
	Username string `json:"username"`
	// Updated user object
	// in:body
	// required:true
	Body mdl.User `json:"body"`
}

// swagger:response userPutResponse
type userPutResponse struct {
}

// swagger:route DELETE /user/{username} user userDelRequest
// Delete user.
// This can only be done by the logged in user.
// responses:
//   400:userDelResponse
//   404:userDelResponse

// swagger:parameters userDelRequest
type userDelRequest struct {
	// The name that needs to be deleted
	// in:path
	// required:true
	Username string `json:"username"`
}

// swagger:response userDelResponse
type userDelResponse struct {
}

// swagger:route POST /user user userRequest
// Create user.
// This can only be done by the logged in user.
// responses:
//   default:userResponse

// swagger:parameters userRequest
type userRequest struct {
	// Created user object
	// in:body
	// required:true
	Body mdl.User `json:"body"`
}

// swagger:response userResponse
type userResponse struct {
}
