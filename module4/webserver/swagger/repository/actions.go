package repository

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"

	mdl "gitlab.com/nichego.takogo.azaza/go-kata/module4/webserver/swagger/models"
)

func comparison(p *Storage, text []byte, storage string) error {
	if len(p.data) == 0 && p.autoIncrementCount == 0 {
		switch storage {
		case "pet":
			var a struct {
				Arr []mdl.Pet `json:"arr"`
			}
			a.Arr = make([]mdl.Pet, 0, 16)
			err := json.Unmarshal(text, &a)
			if err != nil {
				return err
			}
			p.data = make([]interface{}, 0, int(float64(len(a.Arr))*1.7))
			p.primaryKeyIDx = make(map[int64]interface{}, int(float64(len(a.Arr))*1.7))
			for i := range a.Arr {
				p.data = append(p.data, &a.Arr[i])
				p.primaryKeyIDx[a.Arr[i].ID] = p.data[i]
			}
			p.autoIncrementCount = int64(len(p.data))
		case "store":
			var a struct {
				Arr []mdl.Order `json:"arr"`
			}
			a.Arr = make([]mdl.Order, 0, 16)
			err := json.Unmarshal(text, &a)
			if err != nil {
				return err
			}
			p.data = make([]interface{}, 0, int(float64(len(a.Arr))*1.7))
			p.primaryKeyIDx = make(map[int64]interface{}, int(float64(len(a.Arr))*1.7))
			for i := range a.Arr {
				p.data = append(p.data, &a.Arr[i])
				p.primaryKeyIDx[a.Arr[i].ID] = p.data[i]
			}
			p.autoIncrementCount = int64(len(p.data))
		case "user":
			var a struct {
				Arr []mdl.User `json:"arr"`
			}
			a.Arr = make([]mdl.User, 0, 16)
			err := json.Unmarshal(text, &a)
			if err != nil {
				return err
			}
			p.data = make([]interface{}, 0, int(float64(len(a.Arr))*1.7))
			p.primaryKeyIDx = make(map[int64]interface{}, int(float64(len(a.Arr))*1.7))
			for i := range a.Arr {
				p.data = append(p.data, &a.Arr[i])
				p.primaryKeyIDx[a.Arr[i].ID] = p.data[i]
			}
			p.autoIncrementCount = int64(len(p.data))
		}
	}
	return nil
}

func getText(storage string) ([]byte, int64, error) {
	file, err := os.OpenFile("storage_files/storage_"+storage+".json", os.O_RDONLY, 0)
	if err != nil {
		return nil, 0, err
	}
	b := make([]byte, 32)
	text := make([]byte, 0, 256)
	var n int
	var count int64
	for err == nil {
		n, err = file.ReadAt(b, count)
		count += int64(n)
		text = append(text, b[:n]...)
	}
	file.Close()
	if err != io.EOF {
		return text, count, err
	}
	return text, count, nil
}

func Update2(in interface{}, p *Storage) error {
	var storage string
	parts := in.([]interface{})
	storage = parts[2].(string)
	text, count, err := getText(storage)
	if err != nil {
		return err
	}
	if count == 0 {
		return fmt.Errorf("empty file")
	}
	err = comparison(p, text, storage)
	if err != nil {
		return err
	}
	js := make([]byte, 0, 16)
	var founded bool
	switch storage {
	case "pet":
		s := parts[1].(string)
		ID, err := strconv.Atoi(s)
		if err != nil {
			return err
		}
		for i := range p.data {
			if p.data[i].(*mdl.Pet).ID == int64(ID) {
				str := string(parts[0].([]byte))
				slc := strings.Split(str, "=")
				slc2 := strings.Split(slc[1], "&")
				if slc[2] != "available" && slc[2] != "pending" && slc[2] != "sold" {
					return fmt.Errorf("invalid status")
				}
				p.data[i].(*mdl.Pet).Name = slc2[0]
				p.data[i].(*mdl.Pet).Status = slc[2]
				founded = true
				break
			}
		}
		if !founded {
			return fmt.Errorf("not found")
		}
		arr := make([]mdl.Pet, 0, 32)
		for i := range p.data {
			arr = append(arr, *p.data[i].(*mdl.Pet))
		}
		js, err = json.Marshal(arr)
		if err != nil {
			return err
		}
	case "user":
		username := parts[1].(string)
		for i := range p.data {
			if p.data[i].(*mdl.User).Username == username {
				slc := parts[0].([]byte)
				var u mdl.User
				err = json.Unmarshal(slc, &u)
				if err != nil {
					return err
				}
				id := p.data[i].(*mdl.User).ID
				for j := range p.data {
					if p.data[j].(*mdl.User).Username == u.Username && j != i {
						return fmt.Errorf("this username is already taken")
					}
				}
				*p.data[i].(*mdl.User) = u
				p.data[i].(*mdl.User).ID = id
				founded = true
				break
			}
		}
		if !founded {
			return fmt.Errorf("not found")
		}
		arr := make([]mdl.User, 0, 32)
		for i := range p.data {
			arr = append(arr, *p.data[i].(*mdl.User))
		}
		js, err = json.Marshal(arr)
		if err != nil {
			return err
		}
	}
	err = os.Remove("storage_files/storage_" + storage + ".json")
	if err != nil {
		return err
	}
	file, err := os.OpenFile("storage_files/storage_"+storage+".json", os.O_WRONLY|os.O_CREATE, 0777)
	if err != nil {
		return err
	}
	defer file.Close()
	count = 0
	n := 0
	n, err = file.WriteAt([]byte("{\"arr\":[]}"), count)
	if err != nil {
		return err
	}
	count += int64(n) - 3
	n, err = file.WriteAt(js, count)
	if err != nil {
		return err
	}
	count += int64(n)
	_, err = file.WriteAt([]byte("}"), count)
	if err != nil {
		return err
	}
	return nil
}
