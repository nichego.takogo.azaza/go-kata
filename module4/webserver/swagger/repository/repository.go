package repository

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strconv"
	"sync"

	mdl "gitlab.com/nichego.takogo.azaza/go-kata/module4/webserver/swagger/models"
)

type Storager interface {
	Create(interface{}) error
	Update(interface{}) error
	Delete(ID interface{}) error
	GetByID(ID interface{}) (interface{}, error)
	GetList(storage string) []interface{}
}

type Storage struct {
	data               []interface{}
	primaryKeyIDx      map[int64]interface{}
	autoIncrementCount int64
	sync.Mutex
}

func NewStorage() *Storage {
	return &Storage{
		data:          make([]interface{}, 0, 16),
		primaryKeyIDx: make(map[int64]interface{}, 16),
	}
}

func (p *Storage) Create(in interface{}) error {
	p.Lock()
	defer p.Unlock()
	var storage string
	switch in.(type) {
	case mdl.Pet:
		storage = "pet"
	case mdl.Order:
		storage = "store"
	case mdl.User:
		storage = "user"
	}
	file, err := os.OpenFile("storage_files/storage_"+storage+".json", os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		return err
	}
	defer file.Close()
	b := make([]byte, 32)
	text := make([]byte, 0, 256)
	var n int
	var count int64
	for err == nil {
		n, err = file.ReadAt(b, count)
		count += int64(n)
		text = append(text, b[:n]...)
	}
	if err != io.EOF {
		return err
	}
	if count == 0 {
		n, err = file.WriteAt([]byte("{\"arr\":[]}"), count)
		if err != nil {
			return err
		}
		count += int64(n)
		switch storage {
		case "pet":
			obj := in.(mdl.Pet)
			obj.ID = 0
			if obj.Status != "available" && obj.Status != "pending" && obj.Status != "sold" {
				return fmt.Errorf("invalid status")
			}
			p.data = append(p.data, &obj)
			p.primaryKeyIDx[obj.ID] = &obj
			p.autoIncrementCount++
			in = obj
		case "store":
			obj := in.(mdl.Order)
			obj.ID = 0
			if obj.Status != "available" && obj.Status != "pending" && obj.Status != "sold" {
				return fmt.Errorf("invalid status")
			}
			p.data = append(p.data, &obj)
			p.primaryKeyIDx[obj.ID] = &obj
			p.autoIncrementCount++
			in = obj
		case "user":
			obj := in.(mdl.User)
			obj.ID = 0
			p.data = append(p.data, &obj)
			p.primaryKeyIDx[obj.ID] = &obj
			p.autoIncrementCount++
			in = obj
		}
	} else {
		err = comparison(p, text, storage)
		if err != nil {
			return err
		}
		switch storage {
		case "pet":
			obj := in.(mdl.Pet)
			if obj.Status != "available" && obj.Status != "pending" && obj.Status != "sold" {
				return fmt.Errorf("invalid status")
			}
			var x, id int64
			for i := range p.data {
				id = p.data[i].(*mdl.Pet).ID
				if id > x {
					x = id
				}
			}
			obj.ID = x + 1
			p.data = append(p.data, &obj)
			p.primaryKeyIDx[obj.ID] = &obj
			p.autoIncrementCount++
			in = obj
		case "store":
			obj := in.(mdl.Order)
			if obj.Status != "available" && obj.Status != "pending" && obj.Status != "sold" {
				return fmt.Errorf("invalid status")
			}
			var x, id int64
			for i := range p.data {
				id = p.data[i].(*mdl.Order).ID
				if id > x {
					x = id
				}
			}
			obj.ID = x + 1
			p.data = append(p.data, &obj)
			p.primaryKeyIDx[obj.ID] = &obj
			p.autoIncrementCount++
			in = obj
		case "user":
			obj := in.(mdl.User)
			var x, id int64
			for i := range p.data {
				id = p.data[i].(*mdl.User).ID
				if id > x {
					x = id
				}
				if p.data[i].(*mdl.User).Username == obj.Username {
					return fmt.Errorf("username already exist")
				}
			}
			obj.ID = x + 1
			p.data = append(p.data, &obj)
			p.primaryKeyIDx[obj.ID] = &obj
			p.autoIncrementCount++
			in = obj
		}
	}
	js, err := json.Marshal(in)
	if err != nil {
		return err
	}
	if count > 10 {
		js = append([]byte(","), js...)
	}
	count -= 2
	n, err = file.WriteAt(js, count)
	if err != nil {
		return err
	}
	count += int64(n)
	_, err = file.WriteAt([]byte("]}"), count)
	if err != nil {
		return err
	}
	return nil
}

func (p *Storage) Update(v interface{}) error {
	p.Lock()
	defer p.Unlock()
	var storage string
	switch v.(type) {
	case mdl.Pet:
		storage = "pet"
	case mdl.User:
		storage = "user"
	default:
		return Update2(v, p)
	}
	text, count, err := getText(storage)
	if err != nil {
		return err
	}
	if count == 0 {
		return fmt.Errorf("empty file")
	}
	err = comparison(p, text, storage)
	if err != nil {
		return err
	}
	js := make([]byte, 0, 16)
	var founded bool
	switch storage {
	case "pet":
		obj := v.(mdl.Pet)
		for i := range p.data {
			if p.data[i].(*mdl.Pet).ID == obj.ID {
				if obj.Status != "available" && obj.Status != "pending" && obj.Status != "sold" {
					return fmt.Errorf("invalid status")
				}
				p.primaryKeyIDx[obj.ID] = &obj
				p.data[i] = &obj
				founded = true
				break
			}
		}
		if !founded {
			return fmt.Errorf("not found")
		}
		arr := make([]mdl.Pet, 0, 32)
		for i := range p.data {
			arr = append(arr, *p.data[i].(*mdl.Pet))
		}
		js, err = json.Marshal(arr)
		if err != nil {
			return err
		}
	case "user":
		obj := v.(mdl.User)
		for i := range p.data {
			if p.data[i].(*mdl.User).ID == obj.ID {
				p.primaryKeyIDx[obj.ID] = &obj
				p.data[i] = &obj
				founded = true
				break
			}
		}
		if !founded {
			return fmt.Errorf("not found")
		}
		arr := make([]mdl.User, 0, 32)
		for i := range p.data {
			arr = append(arr, *p.data[i].(*mdl.User))
		}
		js, err = json.Marshal(arr)
		if err != nil {
			return err
		}
	}
	err = os.Remove("storage_files/storage_" + storage + ".json")
	if err != nil {
		return err
	}
	file, err := os.OpenFile("storage_files/storage_"+storage+".json", os.O_WRONLY|os.O_CREATE, 0777)
	if err != nil {
		return err
	}
	defer file.Close()
	count = 0
	n := 0
	n, err = file.WriteAt([]byte("{\"arr\":[]}"), count)
	if err != nil {
		return err
	}
	count += int64(n) - 3
	n, err = file.WriteAt(js, count)
	if err != nil {
		return err
	}
	count += int64(n)
	_, err = file.WriteAt([]byte("}"), count)
	if err != nil {
		return err
	}
	return nil
}

func (p *Storage) Delete(ID interface{}) error {
	arr := ID.([]string)
	text, count, err := getText(arr[1])
	if err != nil {
		return err
	}
	err = comparison(p, text, arr[1])
	if err != nil {
		return err
	}
	if count <= 10 {
		return fmt.Errorf("empty file")
	}
	js := make([]byte, 0, 16)
	var founded bool
	switch arr[1] {
	case "pet", "store":
		id, err := strconv.Atoi(arr[0])
		if err != nil {
			return err
		}
		if arr[1] == "pet" {
			for i := range p.data {
				if p.data[i].(*mdl.Pet).ID == int64(id) {
					p.data = append(p.data[:i], p.data[i+1:]...)
					delete(p.primaryKeyIDx, int64(id))
					p.autoIncrementCount--
					founded = true
					break
				}
			}
			if !founded {
				return fmt.Errorf("not found")
			}
			arr := make([]mdl.Pet, 0, 32)
			for i := range p.data {
				arr = append(arr, *p.data[i].(*mdl.Pet))
			}
			js, err = json.Marshal(arr)
			if err != nil {
				return err
			}
		} else {
			for i := range p.data {
				if p.data[i].(*mdl.Order).ID == int64(id) {
					p.data = append(p.data[:i], p.data[i+1:]...)
					delete(p.primaryKeyIDx, int64(id))
					p.autoIncrementCount--
					founded = true
					break
				}
			}
			if !founded {
				return fmt.Errorf("not found")
			}
			arr := make([]mdl.Order, 0, 32)
			for i := range p.data {
				arr = append(arr, *p.data[i].(*mdl.Order))
			}
			js, err = json.Marshal(arr)
			if err != nil {
				return err
			}
		}
	case "user":
		username := arr[0]
		for i := range p.data {
			if p.data[i].(*mdl.User).Username == username {
				delete(p.primaryKeyIDx, p.data[i].(*mdl.User).ID)
				p.data = append(p.data[:i], p.data[i+1:]...)
				p.autoIncrementCount--
				founded = true
				break
			}
		}
		if !founded {
			return fmt.Errorf("not found")
		}
		arr := make([]mdl.User, 0, 32)
		for i := range p.data {
			arr = append(arr, *p.data[i].(*mdl.User))
		}
		js, err = json.Marshal(arr)
		if err != nil {
			return err
		}
	}
	err = os.Remove("storage_files/storage_" + arr[1] + ".json")
	if err != nil {
		return err
	}
	file, err := os.OpenFile("storage_files/storage_"+arr[1]+".json", os.O_WRONLY|os.O_CREATE, 0777)
	if err != nil {
		return err
	}
	defer file.Close()
	count = 0
	n := 0
	n, err = file.WriteAt([]byte("{\"arr\":[]}"), int64(count))
	if err != nil {
		return err
	}
	count += int64(n) - 3
	n, err = file.WriteAt(js, int64(count))
	if err != nil {
		return err
	}
	count += int64(n)
	_, err = file.WriteAt([]byte("}"), int64(count))
	if err != nil {
		return err
	}
	return nil
}

func (p *Storage) GetByID(ID interface{}) (interface{}, error) {
	arr := ID.([]string)
	text, count, err := getText(arr[1])
	if err != nil {
		return nil, err
	}
	err = comparison(p, text, arr[1])
	if err != nil {
		return nil, err
	}
	if count <= 10 {
		return nil, fmt.Errorf("empty file")
	}
	switch arr[1] {
	case "pet", "store":
		id, err := strconv.Atoi(arr[0])
		if err != nil {
			return nil, err
		}
		if arr[1] == "pet" {
			for i := range p.data {
				if p.data[i].(*mdl.Pet).ID == int64(id) {
					return *p.primaryKeyIDx[int64(id)].(*mdl.Pet), nil
				}
			}
		} else {
			for i := range p.data {
				if p.data[i].(*mdl.Order).ID == int64(id) {
					return *p.primaryKeyIDx[int64(id)].(*mdl.Order), nil
				}
			}
		}
	case "user":
		username := arr[0]
		for i := range p.data {
			if p.data[i].(*mdl.User).Username == username {
				return *p.primaryKeyIDx[p.data[i].(*mdl.User).ID].(*mdl.User), nil
			}
		}
	}
	return nil, fmt.Errorf("not found")
}

func (p *Storage) GetList(storage string) []interface{} {
	text, count, err := getText(storage)
	if err != nil {
		return nil
	}
	if count <= 10 {
		return nil
	}
	err = comparison(p, text, storage)
	if err != nil {
		return nil
	}
	arr := make([]interface{}, 0, 32)
	switch storage {
	case "pet":
		for _, v := range p.data {
			arr = append(arr, *v.(*mdl.Pet))
		}
	case "store":
		for _, v := range p.data {
			arr = append(arr, *v.(*mdl.Order))
		}
	case "user":
		for _, v := range p.data {
			arr = append(arr, *v.(*mdl.User))
		}
	}
	return arr
}
