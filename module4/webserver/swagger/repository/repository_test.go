package repository

import (
	"fmt"
	"testing"

	mdl "gitlab.com/nichego.takogo.azaza/go-kata/module4/webserver/swagger/models"
)

var p = NewStorage()
var s = NewStorage()
var u = NewStorage()

func TestCreate(t *testing.T) {
	var err error
	err = p.Create(mdl.Pet{Status: "available"})
	if err != nil {
		t.Error(err)
	}
	err = p.Create(mdl.Pet{})
	if err == nil {
		t.Error(err)
	}
	err = s.Create(mdl.Order{Status: "available"})
	if err != nil {
		t.Error(err)
	}
	err = s.Create(mdl.Order{})
	if err == nil {
		t.Error(err)
	}
	err = u.Create(mdl.User{Username: "name"})
	if err != nil && err.Error() != "username already exist" {
		t.Error(err)
	}
	err = u.Create(mdl.User{Username: "name"})
	if err == nil {
		t.Error(err)
	}
}

func TestUpdate(t *testing.T) {
	var err error
	err = p.Update(mdl.Pet{Status: "available"})
	if err != nil {
		t.Error(err)
	}
	err = p.Update(mdl.Pet{})
	if err == nil {
		t.Error(err)
	}
	err = p.Update([]interface{}{[]byte("name=Name&status=available"), "0", "pet"})
	if err != nil {
		t.Error(err)
	}
	err = u.Update([]interface{}{[]byte("{\"username\":\"SName\"}"), "name", "user"})
	fmt.Println("ERR:", err)
	if err != nil && err.Error() != "this username is already taken" {
		t.Error(err)
	}
	err = u.Update(mdl.User{Username: "NewName"})
	if err != nil && err.Error() != "username already exist" {
		t.Error(err)
	}
	err = u.Update(mdl.User{ID: 74})
	if err == nil {
		t.Error(err)
	}
}

func TestGetByID(t *testing.T) {
	var err error
	_, err = p.GetByID([]string{"0", "pet"})
	if err != nil {
		t.Error(err)
	}
	_, err = p.GetByID([]string{"74", "pet"})
	if err == nil {
		t.Error(err)
	}
	_, err = s.GetByID([]string{"0", "store"})
	if err != nil {
		t.Error(err)
	}
	_, err = s.GetByID([]string{"74", "store"})
	if err == nil {
		t.Error(err)
	}
	_, err = u.GetByID([]string{"NewName", "user"})
	if err != nil {
		t.Error(err)
	}
	_, err = u.GetByID([]string{"nn", "user"})
	if err == nil {
		t.Error(err)
	}
}

func TestGetList(t *testing.T) {
	var in []interface{}
	in = p.GetList("pet")
	if in == nil {
		t.Error("GetList1")
	}
	in = p.GetList("")
	if in != nil {
		t.Error("GetList2")
	}
	in = s.GetList("store")
	if in == nil {
		t.Error("GetList3")
	}
	in = s.GetList("")
	if in != nil {
		t.Error("GetList4")
	}
	in = u.GetList("user")
	if in == nil {
		t.Error("GetList5")
	}
	in = u.GetList("")
	if in != nil {
		t.Error("GetList6")
	}
}

func TestDelete(t *testing.T) {
	var err error
	err = p.Delete([]string{"0", "pet"})
	if err != nil {
		t.Error(err)
	}
	err = p.Delete([]string{"74", "pet"})
	if err == nil {
		t.Error(err)
	}
	err = s.Delete([]string{"0", "store"})
	if err != nil {
		t.Error(err)
	}
	err = s.Delete([]string{"74", "store"})
	if err == nil {
		t.Error(err)
	}
	err = u.Delete([]string{"NewName", "user"})
	if err != nil && err.Error() != "username already exist" {
		t.Error(err)
	}
	err = u.Delete([]string{"nn", "user"})
	if err == nil {
		t.Error(err)
	}
}
