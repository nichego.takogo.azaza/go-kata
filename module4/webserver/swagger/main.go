package main

import (
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"

	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	mdl "gitlab.com/nichego.takogo.azaza/go-kata/module4/webserver/swagger/models"
	rep "gitlab.com/nichego.takogo.azaza/go-kata/module4/webserver/swagger/repository"
)

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
	<style>
		body {
			margin: 0;
		}
	</style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/public/swagger.json?{{.Time}}",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

func swaggerUI(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		return
	}
}

func main() {
	port := ":8080"

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	petController := NewController()
	storeController := NewController()
	userController := NewController()
	r.Route("/pet", func(c chi.Router) {
		c.Post("/", petController.PetCreate)
		c.Put("/", petController.PetUpdate)
		c.Get("/{petId}", petController.GetByID)
		c.Post("/{petId}", petController.Update2)
		c.Delete("/{petId}", petController.Delete)
		c.Post("/{petId}/uploadImage", petController.UploadIMG)
		c.Get("/findByStatus", petController.ByStatus)
	})
	r.Route("/store", func(c chi.Router) {
		c.Post("/order", storeController.StoreCreate)
		c.Get("/order/{orderId}", storeController.GetByID)
		c.Delete("/order/{orderId}", storeController.Delete)
		c.Get("/inventory", storeController.Inventory)
	})
	r.Route("/user", func(c chi.Router) {
		c.Post("/", userController.UserCreate)
		c.Post("/createWithArray", userController.UserCreateArr)
		c.Post("/createWithList", userController.UserCreateArr)
		c.Put("/{username}", userController.Update2)
		c.Get("/{username}", userController.GetByID)
		c.Delete("/{username}", userController.Delete)
	})

	//SwaggerUI
	r.Get("/swagger", swaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("public"))).ServeHTTP(w, r)
	})

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}

type Controller struct { // Pet контроллер
	storage rep.Storager
}

func NewController() *Controller { // конструктор нашего контроллера
	return &Controller{storage: rep.NewStorage()}
}

func (p *Controller) PetCreate(w http.ResponseWriter, r *http.Request) {
	obj, err := MakeObject(ReadJS(r.Body), "pet")
	if err != nil {
		http.Error(w, err.Error(), http.StatusMethodNotAllowed)
		return
	}
	err = p.storage.Create(obj)
	if err != nil {
		http.Error(w, err.Error(), http.StatusMethodNotAllowed)
	}
}
func (p *Controller) StoreCreate(w http.ResponseWriter, r *http.Request) {
	obj, err := MakeObject(ReadJS(r.Body), "store")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = p.storage.Create(obj)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(obj)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}
func (p *Controller) UserCreate(w http.ResponseWriter, r *http.Request) {
	obj, err := MakeObject(ReadJS(r.Body), "user")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = p.storage.Create(obj)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
func (p *Controller) PetUpdate(w http.ResponseWriter, r *http.Request) {
	obj, err := MakeObject(ReadJS(r.Body), "pet")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = p.storage.Update(obj)
	if err != nil {
		http.Error(w, err.Error(), http.StatusMethodNotAllowed)
	}
}
func (p *Controller) Update2(w http.ResponseWriter, r *http.Request) {
	arr := make([]interface{}, 3)
	arr[0] = ReadJS(r.Body)
	param := chi.URLParam(r, "petId")
	if param != "" {
		arr[1] = param
		arr[2] = "pet"
	} else {
		arr[1] = chi.URLParam(r, "username")
		arr[2] = "user"
	}
	err := p.storage.Update(arr)
	if err != nil {
		http.Error(w, err.Error(), http.StatusMethodNotAllowed)
	}
}
func (p *Controller) GetByID(w http.ResponseWriter, r *http.Request) {
	arr := make([]string, 2)
	if v := chi.URLParam(r, "petId"); v != "" {
		arr[0] = v
		arr[1] = "pet"
	} else if v := chi.URLParam(r, "orderId"); v != "" {
		arr[0] = v
		arr[1] = "store"
	} else if v := chi.URLParam(r, "username"); v != "" {
		arr[0] = v
		arr[1] = "user"
	}
	obj, err := p.storage.GetByID(arr)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(obj)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
func (p *Controller) Delete(w http.ResponseWriter, r *http.Request) {
	arr := make([]string, 2)
	if v := chi.URLParam(r, "petId"); v != "" {
		arr[0] = v
		arr[1] = "pet"
	} else if v := chi.URLParam(r, "orderId"); v != "" {
		arr[0] = v
		arr[1] = "store"
	} else if v := chi.URLParam(r, "username"); v != "" {
		arr[0] = v
		arr[1] = "user"
	}
	err := p.storage.Delete(arr)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
	}
}
func (p *Controller) UserCreateArr(w http.ResponseWriter, r *http.Request) {
	arr := ReadJS(r.Body)
	users := make([]mdl.User, 0, 16)
	err := json.Unmarshal(arr, &users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	for _, v := range users {
		err = p.storage.Create(v)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}
}
func (p *Controller) Inventory(w http.ResponseWriter, r *http.Request) {
	m := make(map[string]int, 16)
	arr := p.storage.GetList("pet")
	pets := make([]mdl.Pet, 0, 32)
	for _, v := range arr {
		pets = append(pets, v.(mdl.Pet))
	}
	av := "available"
	pe := "pending"
	so := "sold"
	m[av] = 0
	m[pe] = 0
	m[so] = 0
	for _, v := range pets {
		if v.Status == av {
			m[av]++
		} else if v.Status == pe {
			m[pe]++
		} else if v.Status == so {
			m[so]++
		} else {
			http.Error(w, fmt.Errorf("invalid status").Error(), http.StatusInternalServerError)
			return
		}
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(m)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
func (p *Controller) ByStatus(w http.ResponseWriter, r *http.Request) {
	slc := strings.Split(r.URL.RawQuery, "=")
	if slc[1] != "available" && slc[1] != "pending" && slc[1] != "sold" {
		http.Error(w, fmt.Errorf("invalid status").Error(), http.StatusMethodNotAllowed)
		return
	}
	arr := p.storage.GetList("pet")
	pets := make([]mdl.Pet, 0, 32)
	ans := make([]mdl.Pet, 0, 32)
	for _, v := range arr {
		pets = append(pets, v.(mdl.Pet))
	}
	for _, v := range pets {
		if v.Status == slc[1] {
			ans = append(ans, v)
		}
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(ans)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
func (p *Controller) UploadIMG(w http.ResponseWriter, r *http.Request) {
	img, header, err := r.FormFile("file")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer img.Close()
	slc := strings.Split(header.Filename, ".")
	fname := "IMG_Pet_" + chi.URLParam(r, "petId") + "." + slc[len(slc)-1]
	arr := make([]string, 2)
	arr[0] = chi.URLParam(r, "petId")
	arr[1] = "pet"
	in, err := p.storage.GetByID(arr)
	if err == nil {
		pet := in.(mdl.Pet)
		pet.PhotoUrls = append(pet.PhotoUrls, "storage_files/images/"+fname)
		err = p.storage.Update(pet)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	} else {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	os.Mkdir("storage_files/images/", 0777)
	file, err := os.Create("storage_files/images/" + fname)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = io.Copy(file, img)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
func ReadJS(r io.Reader) []byte {
	b := make([]byte, 32)
	ans := make([]byte, 0, 64)
	var err error
	var n int
	for err == nil {
		n, err = r.Read(b)
		ans = append(ans, b[:n]...)
	}
	return ans
}

func MakeObject(ans []byte, object string) (interface{}, error) {
	var err error
	switch object {
	case "pet":
		var pet mdl.Pet
		err = json.Unmarshal(ans, &pet)
		return pet, err
	case "store":
		var order mdl.Order
		err = json.Unmarshal(ans, &order)
		return order, err
	case "user":
		var user mdl.User
		err = json.Unmarshal(ans, &user)
		return user, err
	}
	return nil, fmt.Errorf("invalid object")
}
