package main

type ListNode3 struct {
	Val  int
	Next *ListNode3
}

func mergeInBetween(list1 *ListNode3, a int, b int, list2 *ListNode3) *ListNode3 {
	twin, twin2 := list1, list1
	for i := 0; i <= b; i++ {
		if twin2.Next != nil {
			twin2 = twin2.Next
		}
	}
	for i := 0; i <= b; i++ {
		if i+1 == a {
			twin.Next = list2
			for twin.Next != nil {
				twin = twin.Next
			}
			twin.Next = twin2
		}
		if twin.Next != nil {
			twin = twin.Next
		}
	}
	return list1
}
