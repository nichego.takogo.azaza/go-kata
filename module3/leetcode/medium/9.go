package main

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	ans := make([]bool, 0, len(l))
	for i := range l {
		ans = append(ans, isArithmetic(nums[l[i]:r[i]+1]))
	}
	return ans
}

func isArithmetic(nums []int) bool {
	if len(nums) == 2 {
		return true
	} else if len(nums) < 2 {
		return false
	}
	var min int
	min = nums[0]
	for _, v := range nums {
		if v < min {
			min = v
		}
	}
	var diff int
	diff = 1<<63 - 1
	for _, v := range nums {
		if v < diff && v > min {
			diff = v
		}
	}
	if diff == 1<<63-1 {
		diff = 0
	}
	diff -= min
	for i := 0; i < len(nums)-1; i++ {
		min += diff
		for j, v := range nums {
			if v == min {
				break
			}
			if j == len(nums)-1 {
				return false
			}
		}
	}
	return true
}
