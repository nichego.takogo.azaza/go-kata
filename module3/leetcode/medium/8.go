package main

func findSmallestSetOfVertices(n int, edges [][]int) []int {
	m := make(map[int]int, n)
	var v int
	for i := 0; i < len(edges); i++ {
		v = edges[i][1]
		if _, ok := m[v]; !ok {
			m[v] = v
		}
	}
	ans := make([]int, 0, n-len(m))
	for i := 0; i < n; i++ {
		if _, ok := m[i]; !ok {
			ans = append(ans, i)
		}
	}
	return ans
}
