package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

var curentPosition, deepest, ans int

func deepestLeavesSum(root *TreeNode) int {
	if root.Left == nil && root.Right == nil {
		return root.Val
	}
	deepest = 0
	ans = 0
	check(root)
	curentPosition = 0
	sum(root)
	return ans
}

func check(root *TreeNode) {
	curentPosition++
	if curentPosition > deepest {
		deepest = curentPosition
	}
	if !(root.Left == nil) {
		check(root.Left)
	}
	if !(root.Right == nil) {
		check(root.Right)
	}
	curentPosition--
	return
}
func sum(root *TreeNode) {
	curentPosition++
	if curentPosition == deepest {
		ans += root.Val
		curentPosition--
		return
	}
	if !(root.Left == nil) {
		sum(root.Left)
	}
	if !(root.Right == nil) {
		sum(root.Right)
	}
	curentPosition--
	return
}
