package main

func countPoints(points [][]int, queries [][]int) []int {
	answer := make([]int, len(queries))
	for i, v := range queries {
		for _, v2 := range points {
			if check2(v2[0], v2[1], v[0], v[1], v[2]) {
				answer[i]++
			}
		}
	}
	return answer
}

func check2(x, y, a, b, r int) bool {
	if (x-a)*(x-a)+(y-b)*(y-b) <= r*r {
		return true
	}
	return false
}
