package main

type TreeNode2 struct {
	Val   int
	Left  *TreeNode2
	Right *TreeNode2
}

func bstToGst(root *TreeNode2) *TreeNode2 {
	n := new(int)
	walk3(root, n)
	return root
}

func walk3(root *TreeNode2, n *int) {
	if root.Right != nil {
		walk3(root.Right, n)
	}
	root.Val += *n
	*n = root.Val
	if root.Left != nil {
		walk3(root.Left, n)
	}
	return
}
