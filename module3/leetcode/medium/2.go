package main

func sortTheStudents(score [][]int, k int) [][]int {
	rating := make([]int, 0, len(score))
	for _, v := range score {
		rating = append(rating, v[k])
	}
	var max int
	for i := 0; i < len(rating); i++ {
		max = i
		for t := i; t < len(rating); t++ {
			if rating[max] < rating[t] {
				max = t
			}
		}
		if max != i {
			rating[i], rating[max] = rating[max], rating[i]
			score[i], score[max] = score[max], score[i]
		}
	}
	return score
}
