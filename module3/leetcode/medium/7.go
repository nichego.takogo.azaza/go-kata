package main

type TreeNode4 struct {
	Val   int
	Left  *TreeNode4
	Right *TreeNode4
}

func balanceBST(root *TreeNode4) *TreeNode4 {
	slc := make([]int, 0, 64)
	twin := root
	walk4(twin, &slc)
	ans := &TreeNode4{}
	fill(ans, slc)
	return ans
}

func walk4(root *TreeNode4, slc *[]int) {
	if root.Left != nil {
		walk4(root.Left, slc)
	}
	*slc = append(*slc, root.Val)
	if root.Right != nil {
		walk4(root.Right, slc)
	}
}

func fill(root *TreeNode4, slc []int) {
	if len(slc) == 1 {
		root.Val = slc[0]
		return
	} else if len(slc) > 1 {
		var mid int
		if len(slc)%2 == 0 {
			mid = len(slc)/2 - 1
		} else {
			mid = len(slc) / 2
		}
		root.Val = slc[mid]
		if mid != 0 {
			root.Left = &TreeNode4{}
			fill(root.Left, slc[:mid])
		}
		root.Right = &TreeNode4{}
		fill(root.Right, slc[mid+1:])
	}
}
