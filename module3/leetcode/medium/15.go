package main

func minPartitions(n string) int {
	c := 48
	var ans rune = 0
	for _, v := range n {
		if ans < v {
			ans = v
		}
		if ans == 57 {
			return 9
		}
	}
	return int(ans) - c
}
