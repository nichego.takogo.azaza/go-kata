package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeNodes(head *ListNode) *ListNode {
	ans := make([]*ListNode, 0, 64)
	var number int
	for {
		if head.Next != nil {
			head = head.Next
		} else {
			break
		}
		if head.Val == 0 {
			if number != 0 {
				ans = append(ans, &ListNode{Val: number})
				number = 0
			}
		} else {
			number += head.Val
		}

	}
	for i := 0; i < len(ans)-1; i++ {
		ans[i].Next = ans[i+1]
	}
	return ans[0]
}
