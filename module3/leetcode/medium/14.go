package main

func xorQueries(arr []int, queries [][]int) []int {
	res := make([]int, 0, len(queries))
	for _, v := range queries {
		res = append(res, xor(arr[v[0]:v[1]+1]))
	}
	return res
}

func xor(arr []int) int {
	var curr int
	for _, v := range arr {
		curr = curr ^ v
	}
	return curr
}
