package main

type ListNode2 struct {
	Val  int
	Next *ListNode2
}

func pairSum(head *ListNode) int {
	var count int
	twin := head
	for {
		count++
		if twin.Next != nil {
			twin = twin.Next
		} else {
			break
		}
	}
	slc := make([]int, 0, count/2)
	for i := 0; i < count; i++ {

		if i < count/2 {
			slc = append(slc, head.Val)
		} else {
			slc[i-1-(i-count/2)*2] += head.Val
		}
		if head.Next != nil {
			head = head.Next
		}
	}
	var max int
	for _, v := range slc {
		if max < v {
			max = v
		}
	}
	return max
}
