package main

type TreeNode3 struct {
	Val   int
	Left  *TreeNode3
	Right *TreeNode3
}

func constructMaximumBinaryTree(nums []int) *TreeNode {
	ans := &TreeNode{}
	walk2(nums, ans)
	return ans
}

func maxI(nums []int) int {
	var index, max int
	for i := range nums {
		if max < nums[i] {
			max = nums[i]
			index = i
		}
	}
	return index
}

func walk2(nums []int, ans *TreeNode) {
	index := maxI(nums)
	ans.Val = nums[index]
	if index != 0 {
		left := make([]int, 0, len(nums[:index]))
		left = append(left, nums[:index]...)
		copy(left, nums[:index])
		ans.Left = &TreeNode{}
		walk2(left, ans.Left)
	}
	if index != len(nums)-1 {
		right := make([]int, 0, len(nums[index:])-1)
		right = append(right, nums[index+1:]...)
		copy(right, nums[index+1:])
		ans.Right = &TreeNode{}
		walk2(right, ans.Right)
	}
}
