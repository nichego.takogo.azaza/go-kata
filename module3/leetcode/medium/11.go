package main

type TreeNode5 struct {
	Val   int
	Left  *TreeNode5
	Right *TreeNode5
}

func removeLeafNodes(root *TreeNode5, target int) *TreeNode5 {
	walk(root, target)
	clean(root)
	if root.Left == nil && root.Right == nil && root.Val == -1 {
		root = nil
	}
	return root
}

func walk(root *TreeNode5, target int) {
	if root.Left != nil {
		walk(root.Left, target)
	}
	if root.Right != nil {
		walk(root.Right, target)
	}
	if root.Val == target && root.Left == nil && root.Right == nil {
		root.Val = -1
	} else if root.Val == target && root.Left != nil && root.Left.Val == -1 && root.Right != nil && root.Right.Val == -1 {
		root.Val = -1
	} else if root.Val == target && root.Left != nil && root.Left.Val == -1 && root.Right == nil {
		root.Val = -1
	} else if root.Val == target && root.Right != nil && root.Right.Val == -1 && root.Left == nil {
		root.Val = -1
	}
}

func clean(root *TreeNode5) {
	if root.Left != nil {
		if root.Left.Val == -1 {
			root.Left = nil
		} else {
			clean(root.Left)
		}
	}
	if root.Right != nil {
		if root.Right.Val == -1 {
			root.Right = nil
		} else {
			clean(root.Right)
		}
	}
}
