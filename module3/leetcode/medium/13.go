package main

func maxSum(grid [][]int) int {
	if !(len(grid) > 2 && len(grid[0]) > 2) {
		return 0
	}
	var max, t, y, x int
	for {
		t = hourglass(grid, y, x)
		if max < t {
			max = t
		}
		if len(grid[y]) != x+3 {
			x++
		} else {
			x = 0
			if len(grid) != y+3 {
				y++
			} else {
				return max
			}
		}
	}
	return max
}

func hourglass(grid [][]int, y, x int) (sum int) {
	for t := x + 3; x < t; x++ {
		sum += grid[y][x]
	}
	y++
	x -= 2
	sum += grid[y][x]
	y++
	x--
	for t := x + 3; x < t; x++ {
		sum += grid[y][x]
	}
	return
}
