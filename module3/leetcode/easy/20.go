package main

func kidsWithCandies(candies []int, extraCandies int) []bool {
	var max int
	for _, v := range candies {
		if max < v {
			max = v
		}
	}
	ans := make([]bool, 0, len(candies))
	for _, v := range candies {
		if v+extraCandies >= max {
			ans = append(ans, true)
		} else {
			ans = append(ans, false)
		}
	}
	return ans
}
