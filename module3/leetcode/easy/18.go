package main

func differenceOfSum(nums []int) int {
	var sum1 int
	var sum2 int
	for _, v := range nums {
		sum1 += v
	}
	for _, v := range nums {
		if v > 9 {
			sum2 += numbersSum(v)
		} else {
			sum2 += v
		}
	}
	if sum1 >= sum2 {
		return sum1 - sum2
	}
	return sum2 - sum1
}

func numbersSum(n int) int {
	slc := make([]int, 0, 16)
	for n > 9 {
		slc = append(slc, n%10)
		n /= 10
	}
	slc = append(slc, n)
	var sum int
	for _, v := range slc {
		sum += v
	}
	return sum
}
