package main

func shuffle(nums []int, n int) []int {
	ans := make([]int, len(nums))
	for i, d := 0, 0; i < len(nums)/2; i, d = i+1, d+2 {
		ans[d] = nums[i]
		ans[d+1] = nums[n+i]
	}
	return ans
}
