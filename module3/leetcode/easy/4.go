package main

func buildArray(nums []int) []int {
	ans := make([]int, 0, len(nums))
	for i := range nums {
		ans = append(ans, nums[nums[i]])
	}
	return ans
}
