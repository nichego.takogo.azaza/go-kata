package main

func findKthPositive(arr []int, k int) (ans int) {
	n := 1
	for i := 0; i < len(arr); n++ {
		if arr[i] == n {
			i++
		} else {
			k--
			if k == 0 {
				return n
			}
		}
	}
	return n + k - 1
}
