package main

import "fmt"

func decode(encoded []int, first int) []int {
	res := make([]int, 1, len(encoded)+1)
	res[0] = first
	var xor, curr string
	xor = fmt.Sprintf("%b", first)
	var ans int
	for _, c := range encoded {
		v := fmt.Sprintf("%b", c)
		for ai, bi := len(xor)-1, len(v)-1; ai >= 0 && bi >= 0; ai, bi = ai-1, bi-1 {
			if xor[ai] == v[bi] {
				curr = "0" + curr
			} else {
				curr = "1" + curr
			}
		}
		if len(xor) > len(v) {
			curr = xor[:len(xor)-len(v)] + curr
		} else if len(v) > len(xor) {
			curr = v[:len(v)-len(xor)] + curr
		}
		xor = curr
		ans = 0
		for i, s := len(xor)-1, 0; i >= 0; i, s = i-1, s+1 {
			if string(xor[i]) == "1" {
				ans += pow2(s)
			}
		}
		res = append(res, ans)
		curr = ""
	}
	return res
}

func pow2(p int) int {
	ans := 1
	for i := 0; i < p; i++ {
		ans *= 2
	}
	return ans
}
