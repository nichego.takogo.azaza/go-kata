package main

const n97 = 97

func uniqueMorseRepresentations(words []string) int {
	var morse = [...]string{".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."}
	combinations := make(map[string]bool, len(words)+len(words)/8)
	morseWords := make([]string, 0, len(words))
	var mWord string
	for _, s := range words {
		mWord = ""
		for _, v := range s {
			mWord += morse[v-n97]
		}
		if _, ok := combinations[mWord]; !ok {
			combinations[mWord] = true
			morseWords = append(morseWords, mWord)
		}
	}
	return len(morseWords)
}
