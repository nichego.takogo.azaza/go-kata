package main

func smallerNumbersThanCurrent(nums []int) []int {
	ans := make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		for _, v := range nums {
			if nums[i] > v {
				ans[i]++
			}
		}
	}
	return ans
}
