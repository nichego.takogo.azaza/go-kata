package main

func mostWordsFound(sentences []string) (max int) {
	for _, v := range sentences {
		for i, count := 0, 1; i < len(v); i++ {
			if string(v[i]) == " " {
				count++
			}
			if i == len(v)-1 {
				if max < count {
					max = count
				}
			}
		}
	}
	return
}
