package main

func finalValueAfterOperations(operations []string) int {
	var x int
	for _, oper := range operations {
		for _, sign := range oper {
			if string(sign) == "-" {
				x--
				break
			} else if string(sign) == "+" {
				x++
				break
			}
		}
	}
	return x
}
