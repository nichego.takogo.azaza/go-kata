package main

func interpret(command string) string {
	var ans string
	for i, v := range command {
		if string(v) == "a" {
			ans += "al"
		} else if string(v) == "G" {
			ans += "G"
		} else if string(command[i]) == ")" && string(command[i-1]) == "(" {
			ans += "o"
		}
	}
	return ans
}
