package main

func defangIPaddr(address string) (answer string) {
	for _, v := range address {
		if string(v) == "." {
			answer += "[.]"
		} else {
			answer += string(v)
		}
	}
	return
}
