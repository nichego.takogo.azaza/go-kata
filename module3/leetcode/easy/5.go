package main

func numberOfMatches(n int) (ans int) {
	if n == 0 || n == 1 {
		return
	}
	for n > 1 {
		if n%2 == 0 {
			n /= 2
			ans += n
		} else {
			n = (n-1)/2 + 1
			ans += n - 1
		}
	}
	return
}
