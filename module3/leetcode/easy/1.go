package main

func tribonacci(n int) int {
	if n == 0 {
		return 0
	} else if n == 1 {
		return 1
	}
	arr := [4]int{0, 0, 1, 0}
	p := &arr[3]
	for i := 2; i <= n; i++ {
		*p = 0
		for i := 0; i < len(arr)-1; i++ {
			*p += arr[i]
		}
		for i := 0; i < len(arr)-1; i++ {
			arr[i] = arr[i+1]
		}
	}
	return *p
}
