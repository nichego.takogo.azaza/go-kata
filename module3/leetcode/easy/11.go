package main

func runningSum(nums []int) []int {
	ans := make([]int, 0, len(nums))
	var sum int
	for i := 0; i < len(nums); i++ {
		sum += nums[i]
		ans = append(ans, sum)
	}
	return ans
}
