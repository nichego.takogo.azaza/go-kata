package main

func minimumSum(num int) int {
	numbers := make([]int, 0, 4)
	for num > 0 {
		numbers = append(numbers, num%10)
		num /= 10
	}
	var index, min int
	min = 10
	var numb1, numb2 int
	for t := 0; t < 2; t++ {
		min = 10
		for i := 0; i < len(numbers); i++ {
			if numbers[i] < min {
				min = numbers[i]
				index = i
			}
		}
		if t == 0 {
			numb1 = min * 10
		} else {
			numb2 = min * 10
		}
		numbers = append(numbers[:index], numbers[index+1:]...)
	}
	numb1 += numbers[0]
	numb2 += numbers[1]
	return numb1 + numb2
}
