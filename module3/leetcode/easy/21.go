package main

func subtractProductAndSum(n int) int {
	numbers := make([]int, 0, 20)
	for n > 0 {
		numbers = append(numbers, n%10)
		n /= 10
	}
	var mult, sum int
	mult = 1
	for _, v := range numbers {
		mult *= v
	}
	for _, v := range numbers {
		sum += v
	}
	return mult - sum
}
