package main

func maximumWealth(accounts [][]int) (max int) {
	var sum int
	for i := 0; i < len(accounts); i++ {
		sum = 0
		for j := 0; j < len(accounts[i]); j++ {
			sum += accounts[i][j]
			if j == len(accounts[i])-1 {
				if max < sum {
					max = sum
				}
			}
		}
	}
	return
}
