package main

import "fmt"

type Post struct {
	body        string
	publishDate int64
	next        *Post
}

type Feed struct {
	length int
	start  *Post
	end    *Post
}

func (f *Feed) Append(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
	} else {
		if f.end.publishDate > newPost.publishDate {
			panic("ERROR: Post is not newer than last one")
		}
		f.end.next = newPost
	}
	f.end = newPost
	f.length++
}

func (f *Feed) Remove(publishDate int64) {
	if f.length == 0 {
		panic("ERROR: Empty linked list")
	}
	var previousPost *Post
	curentPost := f.start
	for curentPost.publishDate != publishDate {
		if curentPost.next == nil {
			panic("ERROR: Cant find post on this day")
		}
		previousPost = curentPost
		curentPost = curentPost.next
	}
	if curentPost == f.start {
		f.start = curentPost.next
	} else if curentPost == f.end {
		previousPost.next = nil
	} else {
		previousPost.next = curentPost.next
	}
	f.length--
}

func (f *Feed) Insert(newPost *Post) {
	if f.length == 0 {
		panic("ERROR: Empty linked list")
	}
	var previousPost *Post
	curentPost := f.start
	for curentPost.publishDate <= newPost.publishDate {
		if curentPost.next == nil {
			panic("ERROR: Cant find post on this day")
		}
		previousPost = curentPost
		curentPost = curentPost.next
	}
	previousPost.next = newPost
	newPost.next = curentPost
	f.length++
}

func (f *Feed) Inspect() {
	if f.length == 0 {
		panic("ERROR: Empty linked list")
	}
	fmt.Println("==========================")
	fmt.Println("Feed length:", f.length)
	curentPost := f.start
	for i := 0; i < f.length; i++ {
		fmt.Println("Item:", i, "-", curentPost)
		curentPost = curentPost.next
	}
	fmt.Println("==========================")
}

func main() {
	var a, b, c Post
	a.body = "Text A"
	b.body = "Text B"
	c.body = "Text C"

	a.publishDate = 5
	b.publishDate = 8
	c.publishDate = 12

	fd := &Feed{}
	fd.Append(&a)
	fd.Inspect()

	fd.Append(&b)
	fd.Inspect()

	fd.Append(&c)
	fd.Inspect()

	fd.Remove(5)
	fd.Inspect()

	var d Post = Post{body: "Text D", publishDate: 15}
	fd.Append(&d)
	fd.Inspect()

	var e Post = Post{body: "Text E", publishDate: 10}
	fd.Insert(&e)
	fd.Inspect()
}
