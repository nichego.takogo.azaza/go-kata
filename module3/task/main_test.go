package main

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/brianvoe/gofakeit/v6"
)

type DoubleLinkedList struct {
	head *Node
	tail *Node
	curr *Node
	len  int
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
	Insert(int, Commit) error
	Delete(int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(string) *Node
	Search(string) *Node
	Reverse() *DoubleLinkedList
}

func QuickSort(slc []Commit) []Commit {
	if len(slc) < 2 {
		return slc
	}
	middle := slc[len(slc)/2]
	less := make([]Commit, 0, len(slc)*2/3)
	mid := make([]Commit, 0, len(slc)/2)
	more := make([]Commit, 0, len(slc)*2/3)
	for _, v := range slc {
		switch {
		case v.Date.Unix() < middle.Date.Unix():
			less = append(less, v)
		case v.Date.Unix() == middle.Date.Unix():
			mid = append(mid, v)
		case v.Date.Unix() > middle.Date.Unix():
			more = append(more, v)
		}
	}
	less = QuickSort(less)
	mid = QuickSort(mid)
	more = QuickSort(more)
	less = append(less, mid...)
	less = append(less, more...)
	return less
}

func (d *DoubleLinkedList) LoadData(path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	dec := json.NewDecoder(file)
	slc := make([]Commit, 0, 256)
	err = dec.Decode(&slc)
	if err != nil {
		return err
	}
	file.Close()
	slc = QuickSort(slc)
	for i := 0; i < len(slc); i++ {
		cur := &Node{&slc[i], d.tail, nil}
		if d.len == 0 {
			d.head = cur
			d.tail = cur
			d.curr = cur
		} else {
			d.tail.next = cur
			d.tail = d.tail.next
		}
		d.len++
	}
	return nil
}

func (d *DoubleLinkedList) Len() int {
	return d.len
}

func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

func (d *DoubleLinkedList) Next() *Node {
	if d.curr.next != nil {
		d.curr = d.curr.next
		return d.curr
	}
	return nil
}

func (d *DoubleLinkedList) Prev() *Node {
	if d.curr.prev != nil {
		d.curr = d.curr.prev
		return d.curr
	}
	return nil
}

func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	if d.len < n {
		return fmt.Errorf("Out of range. You want:%d, have:%d", n, d.len)
	}
	current := &Node{}
	diff := d.len - n
	if diff <= n-1 {
		current = d.tail
		for diff > 0 {
			current = current.prev
			diff--
		}
	} else {
		diff = n - 1
		current = d.head
		for diff > 0 {
			current = current.next
			diff--
		}
	}
	data := &Node{
		data: &c,
		prev: nil,
		next: nil,
	}
	if current.next != nil {
		data.prev = current
		data.next = current.next
		current.next.prev = data
		current.next = data
	} else {
		data.prev = current
		current.next = data
	}
	d.len++
	return nil
}

func (d *DoubleLinkedList) Delete(n int) error {
	if d.len < n {
		return fmt.Errorf("Out of range. You want:%d, have:%d", n, d.len)
	}
	var current *Node
	diff := d.len - n
	if diff <= n-1 {
		current = d.tail
		for diff > 0 {
			current = current.prev
			diff--
		}
	} else {
		diff = n - 1
		current = d.head
		for diff > 0 {
			current = current.next
			diff--
		}
	}
	if current == d.head {
		if current.next != nil {
			d.head = current.next
			current.next.prev = nil
		} else {
			d.head = nil
			d.tail = nil
			d.curr = nil
			d.len = 0
			return nil
		}
	} else if current == d.tail {
		d.tail = current.prev
		current.prev.next = nil
	} else {
		current.prev.next = current.next
		current.next.prev = current.prev
	}
	d.len--
	return nil
}

func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.curr == nil {
		return fmt.Errorf("Nil pointer")
	}
	current := d.curr
	if current == d.head {
		if current.next != nil {
			d.head = current.next
			current.next.prev = nil
		} else {
			d.head = nil
			d.tail = nil
			d.curr = nil
			d.len = 0
			return nil
		}
	} else if current == d.tail {
		d.tail = current.prev
		current.prev.next = nil
	} else {
		current.prev.next = current.next
		current.next.prev = current.prev
	}
	d.curr = d.head
	d.len--
	return nil
}

func (d *DoubleLinkedList) Index() (int, error) {
	if d.curr == nil {
		return 0, fmt.Errorf("Nil pointer")
	}
	count := 1
	current := d.head
	for current != d.curr {
		current = current.next
		count++
		if current != d.curr && current.next == nil {
			return 0, fmt.Errorf("Cant find node")
		}
	}
	return count, nil
}

func (d *DoubleLinkedList) Pop() *Node {
	if d.len == 0 {
		return nil
	} else if d.len > 1 {
		a := d.tail
		d.tail = d.tail.prev
		d.tail.next = nil
		d.len--
		return a
	} else {
		d.head = nil
		d.tail = nil
		d.curr = nil
		d.len = 0
		return nil
	}
}

func (d *DoubleLinkedList) Shift() *Node {
	if d.len == 0 {
		return nil
	} else if d.len > 1 {
		a := d.head
		d.head = d.head.next
		d.head.prev = nil
		d.len--
		return a
	} else {
		d.head = nil
		d.tail = nil
		d.curr = nil
		d.len = 0
		return nil
	}
}

func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	current := d.head
	for current.next != nil {
		if current.data.UUID == uuID {
			return current
		}
		current = current.next
	}
	return nil
}

func (d *DoubleLinkedList) Search(message string) *Node {
	current := d.head
	for current != nil {
		if current.data.Message == message {
			return current
		}
		current = current.next
	}
	return nil
}

func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	if d.len > 1 {
		d.head, d.tail = d.tail, d.head
		current := d.head
		for current != nil {
			current.next, current.prev = current.prev, current.next
			current = current.next
		}
	}
	return d
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func GenerateJSON(n int) []byte {
	slc := make([]Commit, 0, n)
	for i := 0; i < n; i++ {
		slc = append(slc, Commit{gofakeit.Paragraph(1, 1, 12, "\n"), gofakeit.UUID(), gofakeit.Date()})
	}
	data, _ := json.Marshal(slc)
	return data
}

func TestLen(t *testing.T) {
	d := &DoubleLinkedList{}
	test := [...]int{4, 8, 7, 10, 0}
	for _, v := range test {
		d.len = v
		a := d.Len()
		if v != a {
			t.Errorf("ERROR! want:%d, have:%d", v, a)
		}
	}
}

func BenchmarkLen(b *testing.B) {
	d := &DoubleLinkedList{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.len = i
		d.Len()
	}
}

func TestCurrent(t *testing.T) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	for i := 0; i < 7; i++ {
		v := d.curr
		a := d.Current()
		if v != a {
			t.Errorf("ERROR! want:%v, have:%v", v, a)
		}
		d.Next()
	}
}

func BenchmarkCurrent(b *testing.B) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Current()
	}
}

func TestNext(t *testing.T) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	for i := 0; i < 7; i++ {
		v := d.curr.next
		a := d.Next()
		if v != a {
			t.Errorf("ERROR! want:%v, have:%v", v, a)
		}
	}
}

func BenchmarkNext(b *testing.B) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Next()
	}
}

func TestPrev(t *testing.T) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	d.curr = d.tail
	for i := 0; i < 7; i++ {
		v := d.curr.prev
		a := d.Prev()
		if v != a {
			t.Errorf("ERROR! want:%v, have:%v", v, a)
		}
	}
}

func BenchmarkPrev(b *testing.B) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	d.curr = d.tail
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Prev()
	}
}

func TestLoadData(t *testing.T) {
	d := &DoubleLinkedList{}
	err := d.LoadData("test.json")
	if err != nil {
		t.Error(err)
	}
	for i := 0; i < 30; i++ {
		a := d.curr.data.Date.Unix()
		b := d.curr.next.data.Date.Unix()
		if a > b {
			t.Errorf("ERROR! want more than:%d, have:%d", a, b)
		}
		d.Next()
	}
}

func BenchmarkLoadData(b *testing.B) {
	d := &DoubleLinkedList{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.LoadData("test.json")
	}
}

func TestInsert(t *testing.T) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	for i := 0; i < 20; i++ {
		err := d.Insert(i, Commit{})
		if err != nil {
			t.Error(err)
		}
	}
}

func BenchmarkInsert(b *testing.B) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Insert(0, Commit{})
	}
}

func TestDelete(t *testing.T) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	for i := 0; i < 20; i++ {
		err := d.Delete(i)
		if err != nil {
			t.Error(err)
		}
	}
}

func BenchmarkDelete(b *testing.B) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Delete(0)
		if d.len < 2 {
			b.StopTimer()
			d.LoadData("test.json")
			b.StartTimer()
		}
	}
}

func TestDeleteCurrent(t *testing.T) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	for i := 0; i < 20; i++ {
		err := d.DeleteCurrent()
		if err != nil {
			t.Error(err)
		}
	}
}

func BenchmarkDeleteCurrent(b *testing.B) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.DeleteCurrent()
		if d.len < 2 {
			b.StopTimer()
			d.LoadData("test.json")
			b.StartTimer()
		}
	}
}

func TestIndex(t *testing.T) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	for i := 0; i < 20; i++ {
		_, err := d.Index()
		if err != nil {
			t.Error(err)
		}
		d.curr = d.Next()
	}
}

func BenchmarkIndex(b *testing.B) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Index()
	}
}

func TestPop(t *testing.T) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	for i := 0; i < 20; i++ {
		v := d.tail
		a := d.Pop()
		if v != a {
			t.Errorf("ERROR! want:%v, have:%v", v, a)
		}
	}
}

func BenchmarkPop(b *testing.B) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Pop()
		if d.len < 2 {
			b.StopTimer()
			d.LoadData("test.json")
			b.StartTimer()
		}
	}
}

func TestShift(t *testing.T) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	for i := 0; i < 20; i++ {
		v := d.head
		a := d.Shift()
		if v != a {
			t.Errorf("ERROR! want:%v, have:%v", v, a)
		}
	}
}

func BenchmarkShift(b *testing.B) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Shift()
		if d.len < 2 {
			b.StopTimer()
			d.LoadData("test.json")
			b.StartTimer()
		}
	}
}

func TestSearchUUID(t *testing.T) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	for i := 0; i < 20; i++ {
		v := d.SearchUUID("6956d3f4-875b-11ed-8150-acde48001122")
		if v != nil && v.data.UUID != "6956d3f4-875b-11ed-8150-acde48001122" {
			t.Errorf("ERROR! SearchUUID")
		}
		d.Next()
	}
}

func BenchmarkSearchUUID(b *testing.B) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.SearchUUID("6956d3f4-875b-11ed-8150-acde48001122")
	}
}

func TestSearch(t *testing.T) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	for i := 0; i < 20; i++ {
		v := d.Search("We need to program the bluetooth ADP protocol!")
		if v != nil && v.data.Message != "We need to program the bluetooth ADP protocol!" {
			t.Errorf("ERROR! Search")
		}
		d.Next()
	}
}

func BenchmarkSearch(b *testing.B) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Search("We need to program the bluetooth ADP protocol!")
	}
}

func TestReverse(t *testing.T) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	d.Reverse()
	if d.head.data.Date.Unix() < d.tail.data.Date.Unix() {
		t.Errorf("ERROR! Reverse")
	}
}

func BenchmarkReverse(b *testing.B) {
	d := &DoubleLinkedList{}
	d.LoadData("test.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Reverse()
	}
}
