package main

import (
	"fmt"
)

type AirConditioner interface {
	toggle() bool
	set(int) bool
}

type RealAirConditioner struct {
	isOn        bool
	temperature int
}

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

type AirConditionerProxy struct {
	adapter       AirConditionerAdapter
	authenticated bool
}

func NewAirConditionerProxy(authenticated bool) AirConditionerProxy {
	return AirConditionerProxy{
		adapter:       AirConditionerAdapter{&RealAirConditioner{false, 25}},
		authenticated: authenticated,
	}
}

func (a AirConditionerAdapter) toggle() bool {
	b := a.airConditioner.toggleRCondit()
	return b
}
func (a AirConditionerAdapter) set(t int) bool {
	b := a.airConditioner.setRCondit(t)
	return b
}
func (a *RealAirConditioner) toggleRCondit() bool {
	if a.isOn {
		fmt.Println("Conditioner turned off")
		a.isOn = false
		return false
	}
	fmt.Println("Conditioner turned on")
	a.isOn = true
	return true
}
func (a *RealAirConditioner) setRCondit(t int) bool {
	if a.isOn {
		fmt.Println("The air conditioner is set to a new temperature:", t)
		a.temperature = t
		return true
	}
	fmt.Println("Cant set new temperature, turn on conditioner first")
	return false
}
func (a *AirConditionerProxy) toggle() bool {
	if a.authenticated {
		fmt.Print("Access allowed: ")
		return a.adapter.toggle()
	} else {
		fmt.Println("Access denied: authentication required to turn on/off the air conditioner")
		return false
	}
}
func (a *AirConditionerProxy) set(t int) bool {
	if a.authenticated {
		fmt.Print("Access allowed: ")
		return a.adapter.set(t)
	} else {
		fmt.Println("Access denied: authentication required to set the temperature of the air conditioner")
		return false
	}
}

func main() {
	cond1 := NewAirConditionerProxy(true)
	cond2 := NewAirConditionerProxy(false)
	cond1.toggle()
	cond1.set(34)
	cond1.toggle()
	cond1.set(34)
	cond2.toggle()
	cond2.set(34)
	cond2.toggle()
	cond2.set(34)
}
