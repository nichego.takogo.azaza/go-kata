package main

import (
	"fmt"
)

type PricingStrategy interface {
	Calculate(Order) float64
}

type Order struct {
	name   string
	price  float64
	number int
}

type RegularPricing struct {
}

type SalePricing struct {
	name    string
	percent float64
}

func (r RegularPricing) Calculate(o Order) float64 {
	return o.price * float64(o.number)
}

func (r SalePricing) Calculate(o Order) float64 {
	return (o.price * float64(o.number)) / 100 * (100 - r.percent)
}

func main() {
	priceStrat := make([]PricingStrategy, 0, 20)
	ord := make([]Order, 0, 10)
	for i := 0; i < 7; i++ {
		priceStrat = append(priceStrat, RegularPricing{})
		priceStrat = append(priceStrat, SalePricing{name: "", percent: float64(i * 5)})
		ord = append(ord, Order{name: "", price: float64(i * 80), number: i})
	}
	for i := 0; i < 7; i++ {
		fmt.Printf("Total cost with %T strategy: %.2f\n", priceStrat[i*2], priceStrat[i*2].Calculate(ord[i]))
		fmt.Printf("Total cost with %T strategy: %.2f\n\n", priceStrat[i*2+1], priceStrat[i*2+1].Calculate(ord[i]))
	}
}
