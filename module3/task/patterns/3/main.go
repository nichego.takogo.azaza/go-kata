package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type WeatherAPI interface {
	GetTemperature(location string) int
	GetHumidity(location string) int
	GetWindSpeed(location string) int
}

type CityCoord struct {
	Lat float64 `json:"lat"`
	Lon float64 `json:"lon"`
}

type CityWeather struct {
	Main map[string]map[string]float64 `json:"main"`
	Wind map[string]map[string]float64 `json:"wind"`
}

type OpenWeatherAPI struct {
	client *http.Client
	apiKey string
}

func (o *OpenWeatherAPI) GetTemperature(location string) int {
	lat, lon := getLatLon(location, o.apiKey)
	get := fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&appid=%s", lat, lon, o.apiKey)
	resp, err := o.client.Get(get)
	defer resp.Body.Close()
	var c int
	str := make([]byte, 0, 32)
	b := make([]byte, 20)
	for err == nil {
		c, err = resp.Body.Read(b)
		str = append(str, b[:c]...)
	}
	var ans CityWeather
	json.Unmarshal(str, &ans.Main)
	ans2 := int(ans.Main["main"]["temp"]) - 273
	return ans2
}

func (o *OpenWeatherAPI) GetHumidity(location string) int {
	lat, lon := getLatLon(location, o.apiKey)
	get := fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&appid=%s", lat, lon, o.apiKey)
	resp, err := o.client.Get(get)
	defer resp.Body.Close()
	var c int
	str := make([]byte, 0, 32)
	b := make([]byte, 20)
	for err == nil {
		c, err = resp.Body.Read(b)
		str = append(str, b[:c]...)
	}
	var ans CityWeather
	json.Unmarshal(str, &ans.Main)
	ans2 := int(ans.Main["main"]["humidity"])
	return ans2
}

func (o *OpenWeatherAPI) GetWindSpeed(location string) int {
	lat, lon := getLatLon(location, o.apiKey)
	get := fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&appid=%s", lat, lon, o.apiKey)
	resp, err := o.client.Get(get)
	defer resp.Body.Close()
	var c int
	str := make([]byte, 0, 32)
	b := make([]byte, 20)
	for err == nil {
		c, err = resp.Body.Read(b)
		str = append(str, b[:c]...)
	}
	var ans CityWeather
	json.Unmarshal(str, &ans.Wind)
	ans2 := int(ans.Wind["wind"]["speed"])
	return ans2
}

type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(location string) (int, int, int) {
	temperature := w.weatherAPI.GetTemperature(location)
	humidity := w.weatherAPI.GetHumidity(location)
	windSpeed := w.weatherAPI.GetWindSpeed(location)

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	client := http.Client{}
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{client: &client, apiKey: apiKey},
	}
}

func main() {
	weatherFacade := NewWeatherFacade("b0c024bae61fc754c09d9ac8e42d1ddf")
	cities := []string{"Москва", "Санкт-Петербург", "Казань", "Якутск"}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %d\n", temperature)
		fmt.Printf("Humidity in "+city+": %d\n", humidity)
		fmt.Printf("Wind speed in "+city+": %d\n\n", windSpeed)
	}
}

func getLatLon(location, apiKey string) (lat, lon string) {
	client := new(http.Client)
	get := fmt.Sprintf("http://api.openweathermap.org/geo/1.0/direct?q=%s&limit=1&appid=%s", location, apiKey)
	resp, err := client.Get(get)
	defer resp.Body.Close()
	var c int
	str := make([]byte, 0, 64)
	b := make([]byte, 20)
	for err == nil {
		c, err = resp.Body.Read(b)
		str = append(str, b[:c]...)
	}
	latlon := make([]CityCoord, 0, 32)
	json.Unmarshal(str, &latlon)
	lat = fmt.Sprint(latlon[0].Lat)
	lon = fmt.Sprint(latlon[0].Lon)
	return
}
