package service

import (
	"fmt"
)

type Todos struct {
	Slc []Todo `json:"Todos"`
}

type Todo struct {
	Title      string `json:"title"`
	IsComplete bool   `json:"isComplete"`
}

type TodoService interface {
	ListTodos() ([]Todo, error)
	CreateTodo(string)
	CompleteTodo(Todo) error
	RemoveTodo(Todo) error
}

func NewTodoList() Todos {
	slc := make([]Todo, 0, 16)
	return Todos{slc}
}

func (t *Todos) ListTodos() ([]Todo, error) {
	if len(t.Slc) == 0 {
		return nil, fmt.Errorf("empty array")
	} else {
		return t.Slc, nil
	}
}
func (t *Todos) CreateTodo(title string) {
	t.Slc = append(t.Slc, Todo{title, false})
}
func (t *Todos) CompleteTodo(title string) error {
	for i, v := range t.Slc {
		if v.Title == title {
			if v.IsComplete == false {
				(t.Slc)[i].IsComplete = true
				return nil
			} else {
				return fmt.Errorf("task already complited")
			}
		}
	}
	return fmt.Errorf("cant find this task")
}
func (t *Todos) RemoveTodo(title string) error {
	for i, v := range t.Slc {
		if v.Title == title {
			t.Slc = append(t.Slc[:i], t.Slc[i+1:]...)
			return nil
		}
	}
	return fmt.Errorf("cant find this task")
}
func (t *Todos) ChangeTodo(title, newTitle string) error {
	for i, v := range t.Slc {
		if v.Title == title {
			t.Slc[i].Title = newTitle
		}
	}
	return fmt.Errorf("cant find this task")
}
