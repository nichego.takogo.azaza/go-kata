package main

import (
	"fmt"

	rep "gitlab.com/nichego.takogo.azaza/go-kata/module3/clean_architecture/service/repo"
	serv "gitlab.com/nichego.takogo.azaza/go-kata/module3/clean_architecture/service/service"
)

func main() {
	list := serv.NewTodoList()
	list.CreateTodo("text1")
	list.CreateTodo("text2")
	list.CreateTodo("text3")
	list.CreateTodo("text4")
	list.CreateTodo("text5")
	list.CreateTodo("text6")
	arr, _ := list.ListTodos()
	repo := rep.NewRepository("test.json")
	fmt.Println(repo.Save(arr))
	fmt.Println(repo.FindAll())
	a, _ := repo.Find("text3")
	if a != nil {
		fmt.Println("A:", a.(serv.Todo))
	}
	b, _ := repo.FindAll()
	for _, v := range b {
		fmt.Println(v)
	}
}
