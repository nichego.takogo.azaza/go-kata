package repository

import (
	"encoding/json"
	"fmt"
	"os"

	serv "gitlab.com/nichego.takogo.azaza/go-kata/module3/clean_architecture/service/service"
)

type Repository interface {
	Save(interface{}) error
	Find(string) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	Path string
}

func NewRepository(path string) UserRepository {
	return UserRepository{Path: path}
}

func (r UserRepository) Save(record interface{}) error {
	file, err := os.OpenFile(r.Path, os.O_RDWR|os.O_CREATE, 0)
	if err != nil {
		return err
	}
	defer file.Close()
	var count int64
	b := make([]byte, 50)
	for {
		c, err := file.Read(b)
		count += int64(c)
		if err != nil {
			break
		}
	}
	js, err := json.Marshal(record)
	if err != nil {
		return err
	}
	if count > 0 {
		if js[0] == '[' {
			js = js[1:]
		}
		count -= 2
		file.WriteAt([]byte(","), count)
		count++
	} else {
		file.WriteAt([]byte("{\"Todos\":"), 0)
		count = 9
		if js[0] != '[' {
			file.WriteAt([]byte("["), count)
			count++
		}
	}
	n, _ := file.WriteAt(js, count)
	count += int64(n)
	if js[len(js)-1] != ']' {
		file.WriteAt([]byte("]"), count)
		count++
	}
	file.WriteAt([]byte("}"), count)
	return nil
}

func (r UserRepository) Find(title string) (interface{}, error) {
	file, err := os.OpenFile(r.Path, os.O_RDONLY, 0)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	var c int
	str := make([]byte, 0, 64)
	b := make([]byte, 20)
	for err == nil {
		c, err = file.Read(b)
		str = append(str, b[:c]...)
	}
	var todos serv.Todos
	todos.Slc = make([]serv.Todo, 0, 32)
	json.Unmarshal(str, &todos)
	for _, v := range todos.Slc {
		if v.Title == title {
			return v, nil
		}
	}
	return nil, fmt.Errorf("There is no task:\"%s\"", title)
}

func (r UserRepository) FindAll() ([]interface{}, error) {
	file, err := os.OpenFile(r.Path, os.O_RDONLY, 0)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	var c int
	str := make([]byte, 0, 64)
	b := make([]byte, 20)
	for err == nil {
		c, err = file.Read(b)
		str = append(str, b[:c]...)
	}
	if len(str) == 0 {
		return nil, fmt.Errorf("file is empty")
	}
	var todos serv.Todos
	todos.Slc = make([]serv.Todo, 0, 32)
	json.Unmarshal(str, &todos)
	ans := make([]interface{}, 0, 32)
	for _, v := range todos.Slc {
		ans = append(ans, v)
	}
	return ans, nil
}
