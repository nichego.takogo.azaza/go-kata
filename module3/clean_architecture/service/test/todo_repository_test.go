package test

import (
	"encoding/json"
	"os"
	"testing"

	rep "gitlab.com/nichego.takogo.azaza/go-kata/module3/clean_architecture/service/repo"
	serv "gitlab.com/nichego.takogo.azaza/go-kata/module3/clean_architecture/service/service"
)

func TestSave(t *testing.T) {
	rep := rep.NewRepository("testing.json")
	var err error
	list := serv.NewTodoList()
	list.CreateTodo("text1")
	list.CreateTodo("text2")
	list.CreateTodo("text3")
	list.CreateTodo("text4")
	list.CreateTodo("text5")
	list.CreateTodo("text6")
	arr, _ := list.ListTodos()
	err = rep.Save(arr)
	if err != nil {
		t.Error(err)
	}
	file, err := os.Open(rep.Path)
	if err != nil {
		t.Error(err)
	}
	var c int
	b := make([]byte, 20)
	text := make([]byte, 0, 64)
	for err == nil {
		c, err = file.Read(b)
		text = append(text, b[:c]...)
	}
	if !json.Valid(text) {
		t.Error("json not valid")
	}
}

func TestFind(t *testing.T) {
	rep := rep.NewRepository("testing.json")
	_, err := rep.Find("text1")
	if err != nil {
		t.Error(err)
	}
	_, err = rep.Find("text2")
	if err != nil {
		t.Error(err)
	}
	_, err = rep.Find("text3")
	if err != nil {
		t.Error(err)
	}
	_, err = rep.Find("text4")
	if err != nil {
		t.Error(err)
	}
	_, err = rep.Find("text6")
	if err != nil {
		t.Error(err)
	}
}

func TestFindAll(t *testing.T) {
	rep := rep.NewRepository("testing.json")
	_, err := rep.FindAll()
	if err != nil {
		t.Error(err)
	}
}
