package test

import (
	"testing"

	serv "gitlab.com/nichego.takogo.azaza/go-kata/module3/clean_architecture/service/service"
)

var list serv.Todos

func init() {
	list = serv.NewTodoList()
	list.CreateTodo("text1")
	list.CreateTodo("text2")
	list.CreateTodo("text3")
	list.CreateTodo("text4")
	list.CreateTodo("text5")
	list.CreateTodo("text6")
}

func TestListTodos(t *testing.T) {
	_, err := list.ListTodos()
	if err != nil {
		t.Error(err)
	}
}

func TestCompleteTodo(t *testing.T) {
	err := list.CompleteTodo("text4")
	if err != nil {
		t.Error(err)
	}
}

func TestRemoveTodo(t *testing.T) {
	err := list.RemoveTodo("text4")
	if err != nil {
		t.Error(err)
	}
}
