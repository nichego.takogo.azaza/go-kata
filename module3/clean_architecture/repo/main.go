package main

import (
	"fmt"

	rep "gitlab.com/nichego.takogo.azaza/go-kata/module3/clean_architecture/repo/repository"
)

func main() {
	a := rep.NewUserRepository("test.json")
	//fmt.Println(a.Save([]rep.User{{1, "Zghsth"}, {2, "Udfhrg"}, {3, "Lfsdg"}}))
	fmt.Println(a.FindAll())
}
