package repository

import (
	"encoding/json"
	"os"
	"testing"
)

func TestSave(t *testing.T) {
	rep := NewUserRepository("testing.json")
	var err error
	err = rep.Save(User{3, "Fggef"})
	if err != nil {
		t.Error(err)
	}
	err = rep.Save(User{8, "wefwlekf"})
	if err != nil {
		t.Error(err)
	}
	err = rep.Save(User{14, "KJtgsef"})
	if err != nil {
		t.Error(err)
	}
	err = rep.Save(User{38, "Rwfgjjk"})
	if err != nil {
		t.Error(err)
	}
	err = rep.Save(User{30000, "HRgsgv"})
	if err != nil {
		t.Error(err)
	}
	file, err := os.Open(rep.Path)
	if err != nil {
		t.Error(err)
	}
	var c int
	b := make([]byte, 20)
	text := make([]byte, 0, 64)
	for err == nil {
		c, err = file.Read(b)
		text = append(text, b[:c]...)
	}
	if !json.Valid(text) {
		t.Error("json not valid")
	}
}

func TestFind(t *testing.T) {
	rep := NewUserRepository("testing.json")
	var err error
	var ans interface{}
	ans, err = rep.Find(3)
	if err != nil {
		t.Error(err)
	}
	switch ans.(type) {
	case User:
	default:
		t.Error("wrong type")
	}
	ans, err = rep.Find(8)
	if err != nil {
		t.Error(err)
	}
	switch ans.(type) {
	case User:
	default:
		t.Error("wrong type")
	}
	ans, err = rep.Find(14)
	if err != nil {
		t.Error(err)
	}
	switch ans.(type) {
	case User:
	default:
		t.Error("wrong type")
	}
	ans, err = rep.Find(38)
	if err != nil {
		t.Error(err)
	}
	switch ans.(type) {
	case User:
	default:
		t.Error("wrong type")
	}
	ans, err = rep.Find(30000)
	if err != nil {
		t.Error(err)
	}
	switch ans.(type) {
	case User:
	default:
		t.Error("wrong type")
	}
}

func TestFindAll(t *testing.T) {
	rep := NewUserRepository("testing.json")
	_, err := rep.FindAll()
	if err != nil {
		t.Error(err)
	}
}
