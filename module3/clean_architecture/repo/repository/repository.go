package repository

import (
	"encoding/json"
	"fmt"
	"os"
)

type Repository interface {
	Save(interface{}) error
	Find(int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	Path string
}

type Users struct {
	UsersArr []User `json:"Users"`
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func NewUserRepository(path string) UserRepository {
	return UserRepository{Path: path}
}

func (r UserRepository) Save(record interface{}) error {
	file, err := os.OpenFile(r.Path, os.O_RDWR|os.O_CREATE, 0)
	if err != nil {
		return err
	}
	defer file.Close()
	var count int64
	b := make([]byte, 50)
	for {
		c, err := file.Read(b)
		count += int64(c)
		if err != nil {
			break
		}
	}
	js, err := json.Marshal(record)
	if err != nil {
		return err
	}
	if count > 0 {
		if js[0] == '[' {
			js = js[1:]
		}
		count -= 2
		file.WriteAt([]byte(","), count)
		count++
	} else {
		file.WriteAt([]byte("{\"Users\":"), 0)
		count = 9
		if js[0] != '[' {
			file.WriteAt([]byte("["), count)
			count++
		}
	}
	n, _ := file.WriteAt(js, count)
	count += int64(n)
	if js[len(js)-1] != ']' {
		file.WriteAt([]byte("]"), count)
		count++
	}
	file.WriteAt([]byte("}"), count)
	return nil
}

func (r UserRepository) Find(id int) (interface{}, error) {
	file, err := os.OpenFile(r.Path, os.O_RDONLY, 0)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	var c int
	str := make([]byte, 0, 64)
	b := make([]byte, 20)
	for err == nil {
		c, err = file.Read(b)
		str = append(str, b[:c]...)
	}
	var users Users
	users.UsersArr = make([]User, 0, 32)
	json.Unmarshal(str, &users)
	for _, v := range users.UsersArr {
		if v.ID == id {
			return v, nil
		}
	}
	return nil, fmt.Errorf("There is no user with id:%d", id)
}

func (r UserRepository) FindAll() ([]interface{}, error) {
	file, err := os.OpenFile(r.Path, os.O_RDONLY, 0)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	var c int
	str := make([]byte, 0, 64)
	b := make([]byte, 20)
	for err == nil {
		c, err = file.Read(b)
		str = append(str, b[:c]...)
	}
	var users Users
	users.UsersArr = make([]User, 0, 32)
	json.Unmarshal(str, &users)
	ans := make([]interface{}, 0, 32)
	for _, v := range users.UsersArr {
		ans = append(ans, v)
	}
	return ans, nil
}
