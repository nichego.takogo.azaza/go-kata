package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"time"

	serv "gitlab.com/nichego.takogo.azaza/go-kata/module3/clean_architecture/service/service"
)

func main() {
	list := serv.NewTodoList()
	var chosen int
	chosen = 100
	for chosen != 0 {
		chosen = menu()
		if chosen > 0 {
			fmt.Println("=================")
		}
		switch chosen {
		case 1:
			fmt.Print("Список задач:\n")
			arr, err := list.ListTodos()
			if err != nil {
				fmt.Println("ERROR:", err)
			}
			for i, v := range arr {
				fmt.Print(i+1, ". ", v, "\n")
			}
		case 2:
			fmt.Print("Новая задача: ")
			sc := bufio.NewScanner(os.Stdin)
			sc.Scan()
			list.CreateTodo(sc.Text())
		case 3:
			fmt.Print("Удалить задачу:\n")
			arr, err := list.ListTodos()
			if err != nil {
				fmt.Println("ERROR:", err)
			}
			for i, v := range arr {
				fmt.Print(i+1, ". ", v, "\n")
			}
			n := takeN()
			n--
			if n >= len(arr) || n < 0 {
				fmt.Println("Задачи с номером", n+1, "нет")
				break
			}
			list.RemoveTodo(arr[n].Title)
		case 4:
			fmt.Print("Редактировать задачу:\n")
			arr, err := list.ListTodos()
			if err != nil {
				fmt.Println("ERROR:", err)
			}
			for i, v := range arr {
				fmt.Print(i+1, ". ", v, "\n")
			}
			n := takeN()
			n--
			if n >= len(arr) || n < 0 {
				fmt.Println("Задачи с номером", n+1, "нет")
				break
			}
			fmt.Print(arr[n].Title, " -> ")
			var str string
			fmt.Scanln(&str)
			list.ChangeTodo(arr[n].Title, str)
		case 5:
			fmt.Print("Завершить задачу:\n")
			arr, err := list.ListTodos()
			if err != nil {
				fmt.Println("ERROR:", err)
			}
			for i, v := range arr {
				fmt.Print(i+1, ". ", v, "\n")
			}
			n := takeN()
			n--
			if n >= len(arr) || n < 0 {
				fmt.Println("Задачи с номером", n+1, "нет")
				break
			}
			list.CompleteTodo(arr[n].Title)
		default:
			if chosen != 0 {
				fmt.Println("команды по номеру", chosen, "нет")
			}
		}
		if chosen > 0 {
			fmt.Println("=================")
			time.Sleep(time.Millisecond * 2200)
		}
	}
}

func menu() int {
	fmt.Println("1. Показать список задач.\n2. Добавить задачу.\n3. Удалить задачу.\n4. Редактировать задачу.\n5. Завершить задачу.\n0. Exit.")
	chosen := takeN()
	return chosen
}

func takeN() int {
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	n, err := strconv.Atoi(sc.Text())
	if err != nil {
		fmt.Println("Введено некорректное число")
	}
	return n
}
