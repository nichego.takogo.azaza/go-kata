package main

import (
	"fmt"
)

func typeUint() {
	fmt.Println("uint8:", uint8((1<<8)-1))
	fmt.Println("uint16:", uint16((1<<16)-1))
	fmt.Println("uint32:", uint32((1<<32)-1))
	fmt.Println("uint64:", uint64((1<<64)-1))
}

func main() {
	typeUint()
}
