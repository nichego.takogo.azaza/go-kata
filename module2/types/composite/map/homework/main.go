// You can edit this code!
// Click here and start typing.
package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose1",
			Stars: 16937,
		},
		{
			Name:  "https://github.com/docker/compose2",
			Stars: 25371,
		},
		{
			Name:  "https://github.com/docker/compose3",
			Stars: 36234,
		},
		{
			Name:  "https://github.com/docker/compose4",
			Stars: 48693,
		},
		{
			Name:  "https://github.com/docker/compose5",
			Stars: 53278,
		},
		{
			Name:  "https://github.com/docker/compose6",
			Stars: 63978,
		},
		{
			Name:  "https://github.com/docker/compose7",
			Stars: 71142,
		},
		{
			Name:  "https://github.com/docker/compose8",
			Stars: 80056,
		},
		{
			Name:  "https://github.com/docker/compose9",
			Stars: 94677,
		},
		{
			Name:  "https://github.com/docker/compose10",
			Stars: 107777,
		},
		{
			Name:  "https://github.com/docker/compose11",
			Stars: 118888,
		},
		{
			Name:  "https://github.com/docker/compose12",
			Stars: 124444,
		},
		{
			Name:  "https://github.com/docker/compose13",
			Stars: 136666,
		},
	}
	myMap := map[string]Project{}
	for _, v := range projects {
		myMap[v.Name] = v
	}
	for i, v := range myMap {
		fmt.Println("Key:", i, "   Value:", v)
	}
}
