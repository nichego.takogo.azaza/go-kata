package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
		{
			Name: "Test1",
			Age:  60,
		},
		{
			Name: "Test2",
			Age:  60,
		},
		{
			Name: "Test3",
			Age:  60,
		},
		{
			Name: "Test4",
			Age:  60,
		},
	}
	for i := 0; i < len(users); i++ {
		if users[i].Age > 40 {
			users = append(users[:i], users[i+1:]...)
			i--
		}
	}
	fmt.Println(users)
	users = users[1 : len(users)-1]
	fmt.Println(users)
}
