package main

import (
	"fmt"
	"math/rand"
	"time"
)

const (
	v  = 20
	pi = 3.1415
)

func task1() {
	fmt.Println("Task1:")
	slc := make([]int, 0, v)
	rand.Seed(time.Now().Unix())
	for i := 0; i < v; i++ {
		slc = append(slc, rand.Intn(100)+1)
	}
	fmt.Println(slc)
	var m int = slc[0]
	for i, _ := range slc {
		if slc[i] > m {
			m = slc[i]
		}
	}
	fmt.Println(m)
}
func task2() {
	fmt.Println("Task2:")
	rand.Seed(time.Now().Unix())
	slc := make([]string, 0, v)
	for i := 0; i < cap(slc); i++ {
		slc = append(slc, string(rand.Intn(v-v/3)+97))
	}
	fmt.Println(slc)
	for i := 0; i < len(slc); i++ {
		for k := 0; k < len(slc); k++ {
			if i != k && slc[i] == slc[k] {
				if k+1 >= len(slc) {
					slc = slc[:len(slc)-1]
				} else {
					slc = append(slc[:k], slc[k+1:]...)
					k--
				}
			}
		}
	}
	fmt.Println(slc)
}
func task3() {
	fmt.Println("Task3:")
	rand.Seed(time.Now().Unix())
	slc := make([]string, 0, v)
	for i := 0; i < cap(slc); i++ {
		slc = append(slc, string(rand.Intn(26)+97)+string(rand.Intn(26)+97)+string(rand.Intn(26)+97))
	}
	fmt.Println(slc)
	var num int
	for i := 0; i < len(slc); i++ {
		num = i
		for k := i; k < len(slc); k++ {
			if i != k && slc[k] < slc[num] {
				num = k
			}
		}
		slc[i], slc[num] = slc[num], slc[i]
	}
	fmt.Println(slc)
}
func task4() {
	fmt.Println("Task4:")
	rand.Seed(time.Now().Unix())
	slc, slc2 := make([]int, 0, v), make([]int, 0, v)
	for i := 0; i < cap(slc); i++ {
		slc = append(slc, rand.Intn(25)+1)
		slc2 = append(slc2, rand.Intn(25)+1)
	}
	fmt.Println(slc, slc2)
	slc = append(slc, slc2...)
	fmt.Println(slc)
	for i := 0; i < len(slc); i++ {
		for k := 0; k < len(slc); k++ {
			if i != k && slc[i] == slc[k] {
				if k+1 >= len(slc) {
					slc = slc[:len(slc)-1]
				} else {
					slc = append(slc[:k], slc[k+1:]...)
					k--
				}
			}
		}
	}
	fmt.Println(slc)
}
func task5() {
	fmt.Println("Task5:")
	rand.Seed(time.Now().Unix())
	slc, slc2 := make([]int, 0, v), make([]int, 0, v)
	for i := 0; i < cap(slc); i++ {
		slc = append(slc, rand.Intn(100)+1)
	}
	fmt.Println(slc)
	for i := 0; i < cap(slc2); i++ {
		if slc[i]%2 == 0 {
			slc2 = append(slc2, slc[i])
		}
	}
	fmt.Println(slc2)
}
func task6() {
	fmt.Println("Task6:")
	rand.Seed(time.Now().Unix())
	slc := make([]int, 0, v)
	for i := 0; i < cap(slc); i++ {
		slc = append(slc, rand.Intn(30)+1)
	}
	fmt.Println(slc)
	fmt.Print("Est' li v slice chislo 15: ")
	for i := 0; i < cap(slc); i++ {
		if slc[i] == 15 {
			fmt.Println(true)
			break
		} else if i == cap(slc)-1 {
			fmt.Println(false)
		}
	}
}
func task7() {
	fmt.Println("Task7:")
	rand.Seed(time.Now().Unix())
	slc := make([]int, 0, v)
	for i := 0; i < cap(slc); i++ {
		slc = append(slc, rand.Intn(30)+1)
	}
	fmt.Println(slc)
	fmt.Print("Est' li v slice chislo 2, 7, 15 or 21: ")
	var ch bool
	for i := 0; i < cap(slc); i++ {
		if slc[i] == 15 || slc[i] == 21 || slc[i] == 2 || slc[i] == 7 {
			fmt.Println(true)
			ch = true
			break
		} else if i == cap(slc)-1 {
			fmt.Println(false)
		}
	}
	if ch {
		for i := 0; i < len(slc); i++ {
			if slc[i] != 15 && slc[i] != 21 && slc[i] != 2 && slc[i] != 7 {
				if i+1 == len(slc) {
					slc = slc[:len(slc)-1]
				} else {
					slc = append(slc[:i], slc[i+1:]...)
					i--
				}
			}
		}
		fmt.Println(slc)
	}
}
func task8() {
	fmt.Println("Task8:")
	rand.Seed(time.Now().Unix())
	slc := make([]int, 0, v)
	for i := 0; i < cap(slc); i++ {
		slc = append(slc, rand.Intn(100)+1)
	}
	fmt.Println(slc)
	var m uint8
	fmt.Print("kol-vo simvolov: ")
	fmt.Scan(&m)
	slc2 := slc[:m]
	fmt.Println(slc2)
}
func task9() {
	fmt.Println("Task9:")
	rand.Seed(time.Now().Unix())
	slc, slc2 := make([]int, 0, v), make([]int, 0, v)
	for i := 0; i < cap(slc); i++ {
		slc = append(slc, rand.Intn(100)+1)
	}
	fmt.Println(slc)
	for i := len(slc) - 1; i >= 0; i-- {
		slc2 = append(slc2, slc[i])
	}
	fmt.Println(slc2)
}
func task10() {
	fmt.Println("Task10:")
	rand.Seed(time.Now().Unix())
	slc := make([]int, 0, v)
	for i := 0; i < cap(slc); i++ {
		slc = append(slc, rand.Intn(6))
	}
	fmt.Println(slc)
	for i := 0; i < len(slc); i++ {
		if slc[i] == 0 {
			if i+1 == len(slc) {
				slc = slc[:len(slc)-1]
			} else {
				slc = append(slc[:i], slc[i+1:]...)
				i--
			}
		}
	}
	fmt.Println(slc)
}
func task11() {
	fmt.Println("Task11:")
	rand.Seed(time.Now().Unix())
	slc := make([]string, 0, v)
	for i := 0; i < cap(slc); i++ {
		slc = append(slc, string(rand.Intn(3)+97)+string(rand.Intn(3)+97))
	}
	fmt.Println(slc)
	ff := make(map[string]int, 16)
	var v int
	for i := range slc {
		if ff[slc[i]] == 0 {
			v = 1
			for k := range slc {
				if i != k && slc[i] == slc[k] {
					v++
				}
			}
			ff[slc[i]] = v
		}
	}
	fmt.Println(ff)
}
func task12() {
	fmt.Println("Task12:")
	rand.Seed(time.Now().Unix())
	m1, m2 := make(map[string]int, 5), make(map[string]int, 5)
	ts := ""
	for i := 0; i < 5; {
		ts = string(rand.Intn(3)+97) + string(rand.Intn(3)+97)
		if m1[ts] == 0 {
			m1[ts] = rand.Intn(9) + 1
			i++
		}
	}
	for i := 0; i < 5; {
		ts = string(rand.Intn(3)+97) + string(rand.Intn(3)+97)
		if m2[ts] == 0 {
			m2[ts] = rand.Intn(9) + 1
			i++
		}
	}
	fmt.Println("map1:", m1)
	fmt.Println("map2:", m2)
	m3 := make(map[string]int, 10)
	for i, k := range m1 {
		m3[i] = k
	}
	for i, k := range m2 {
		if m3[i] == 0 {
			m3[i] = k
		} else {
			m3[i] = m3[i] + m2[i]
		}
	}
	fmt.Println("map1 + map2:", m3)
}
func task41() {
	fmt.Println("Task41:")
	var a interface{}
	a = 5
	fmt.Printf("%T\n", a)
	a = ""
	fmt.Printf("%T\n", a)
	a = [2]int{2, 3}
	fmt.Printf("%T\n", a)
	a = []int{3, 4, 5}
	fmt.Printf("%T\n", a)
	a = 'f'
	fmt.Printf("%T\n", a)
}
func task42() {
	fmt.Println("Task42:")
	var a interface{}
	rand.Seed(time.Now().Unix())
	f := rand.Intn(3)
	if f == 0 {
		var x int
		a = x
	} else if f == 1 {
		var x string
		a = x
	} else {
		var x bool
		a = x
	}
	switch a.(type) {
	case string:
		fmt.Printf("type: %T true\n", a)
	default:
		fmt.Printf("type: %T false\n", a)
	}
}

type shape interface {
	area() float32
	perimeter() float32
}
type rectangle struct {
	sideA, sideB uint8
}

func (v rectangle) area() float32 {
	return float32(v.sideA * v.sideB)
}
func (v rectangle) perimeter() float32 {
	return float32(v.sideA*2 + v.sideB*2)
}

type circle struct {
	r uint16
}

func (v circle) area() float32 {
	return float32(float32(v.r*v.r) * pi)
}
func (v circle) perimeter() float32 {
	return float32(float32(v.r) * pi * 2)
}

func task43() {
	fmt.Println("Task43:")
	a := rectangle{5, 7}
	b := circle{6}
	fmt.Println("Rectangle:", a.perimeter(), a.area())
	fmt.Printf("Circle: %.2f %.2f\n", b.perimeter(), b.area())
}
func task46() {
	fmt.Println("Task46:")
	var a, b interface{}
	rand.Seed(time.Now().Unix())
	f := rand.Intn(4)
	r := rand.Intn(4) + 97
	switch f {
	case 0:
		var x int = r
		a = x
	case 1:
		var x float32 = float32(r)
		a = x
	case 2:
		var x string = string(r)
		a = x
	case 3:
		var x bool
		if r <= 98 {
			x = false
		} else {
			x = true
		}
		a = x
	}
	f = rand.Intn(4)
	r = rand.Intn(4) + 97
	switch f {
	case 0:
		var x int = r
		b = x
	case 1:
		var x float32 = float32(r)
		b = x
	case 2:
		var x string = string(r)
		b = x
	case 3:
		var x bool
		if r <= 98 {
			x = false
		} else {
			x = true
		}
		b = x
	}
	fmt.Printf("A: %T, %v\nB: %T, %v\n", a, a, b, b)
	fmt.Println("a == b? ", a == b)
}
func main() {
	task46()
}
