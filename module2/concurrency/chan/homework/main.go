package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const timeoutSec = 30

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}
		wg.Wait()
		fmt.Println("Main closed")
		close(mergedCh)
	}()

	return mergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		t := time.NewTicker(time.Second * timeoutSec)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100):
				time.Sleep(time.Millisecond * 150)
			case <-t.C:
				fmt.Println("TIMEOUT")
				return
			}
		}
	}()

	return out
}

func main() {
	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()

	go func() {
		for num := range out {
			a <- num
		}
		fmt.Println("A closed")
		close(a)
	}()

	go func() {
		for num := range out {
			b <- num
		}
		fmt.Println("B closed")
		close(b)
	}()

	go func() {
		for num := range out {
			c <- num
		}
		fmt.Println("C closed")
		close(c)
	}()

	mainChan := joinChannels(a, b, c)

	for num := range mainChan {
		fmt.Println(num)
	}
}
