package main

import (
	"encoding/json"
	"testing"

	jsoniter "github.com/json-iterator/go"
)

var data = []byte(
	`[
		{
		  "id": 743210,
		  "category": {
			"id": 3,
			"name": "Bengalian"
		  },
		  "name": "Jerod",
		  "photoUrls": [
			"<string>",
			"<string>"
		  ],
		  "tags": [
			{
			  "id": 825,
			  "name": "Rustic"
			}
		  ],
		  "status": "pending"
		},
		{
		  "id": 458843,
		  "category": {
			"id": 2,
			"name": "British"
		  },
		  "name": "Dawson",
		  "photoUrls": [
			"<string>",
			"<string>"
		  ],
		  "tags": [
			{
			  "id": 514,
			  "name": "Fantastic"
			}
		  ],
		  "status": "pending"
		},
		{
		  "id": 228197,
		  "category": {
			"id": 2,
			"name": "British"
		  },
		  "name": "Queenie",
		  "photoUrls": [
			"<string>",
			"<string>"
		  ],
		  "tags": [
			{
			  "id": 349,
			  "name": "Handmade"
			}
		  ],
		  "status": "pending"
		},
		{
		  "id": 633729,
		  "category": {
			"id": 4,
			"name": "Scottish"
		  },
		  "name": "Stella",
		  "photoUrls": [
			"<string>",
			"<string>"
		  ],
		  "tags": [
			{
			  "id": 810,
			  "name": "Licensed"
			}
		  ],
		  "status": "pending"
		},
		{
		  "id": 876112,
		  "category": {
			"id": 2,
			"name": "British"
		  },
		  "name": "Cali",
		  "photoUrls": [
			"<string>",
			"<string>"
		  ],
		  "tags": [
			{
			  "id": 7,
			  "name": "Fantastic"
			}
		  ],
		  "status": "pending"
		},
		{
		  "id": 66158,
		  "category": {
			"id": 3,
			"name": "Bengalian"
		  },
		  "name": "Kallie",
		  "photoUrls": [
			"<string>",
			"<string>"
		  ],
		  "tags": [
			{
			  "id": 417,
			  "name": "Refined"
			}
		  ],
		  "status": "pending"
		},
		{
		  "id": 183910,
		  "category": {
			"id": 2,
			"name": "British"
		  },
		  "name": "Xzavier",
		  "photoUrls": [
			"<string>",
			"<string>"
		  ],
		  "tags": [
			{
			  "id": 980,
			  "name": ""
			}
		  ],
		  "status": "pending"
		},
		{
		  "id": 887855,
		  "category": {
			"id": 2,
			"name": "British"
		  },
		  "name": "Mara",
		  "photoUrls": [
			"<string>",
			"<string>"
		  ],
		  "tags": [
			{
			  "id": 41,
			  "name": "Intelligent"
			}
		  ],
		  "status": "pending"
		},
		{
		  "id": 349804,
		  "category": {
			"id": 4,
			"name": "Scottish"
		  },
		  "name": "Madonna",
		  "photoUrls": [
			"<string>",
			"<string>"
		  ],
		  "tags": [
			{
			  "id": 182,
			  "name": "mew"
			}
		  ],
		  "status": "pending"
		},
		{
		  "id": 921277,
		  "category": {
			"id": 3,
			"name": "Bengalian"
		  },
		  "name": "Jay",
		  "photoUrls": [
			"<string>",
			"<string>"
		  ],
		  "tags": [
			{
			  "id": 646,
			  "name": "mew"
			}
		  ],
		  "status": "pending"
		},
		{
		  "id": 957538,
		  "category": {
			"id": 2,
			"name": "British"
		  },
		  "name": "Paul",
		  "photoUrls": [
			"<string>",
			"<string>"
		  ],
		  "tags": [
			{
			  "id": 726,
			  "name": "mew"
			}
		  ],
		  "status": "pending"
		}
	  ]`)

const (
	String  PhotoURL = "<string>"
	Pending Status   = "pending"
)

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type PhotoURL string

type Status string

type WelcomeElement struct {
	ID        int64      `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []PhotoURL `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    Status     `json:"status"`
}

type Welcome []WelcomeElement

func UnmarshalWelcome(data []byte) (Welcome, error) {
	var r Welcome
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Welcome) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func UnmarshalWelcome2(data []byte) (Welcome, error) {
	var r Welcome
	err := jsoniter.Unmarshal(data, &r)
	return r, err
}

func (r *Welcome) Marshal2() ([]byte, error) {
	return jsoniter.Marshal(r)
}

func BenchmarkJson(b *testing.B) {
	var r Welcome
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		r, _ = UnmarshalWelcome(data)
		r.Marshal()
	}
}

func BenchmarkJsoniter(b *testing.B) {
	var r Welcome
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		r, _ = UnmarshalWelcome2(data)
		r.Marshal2()
	}
}
