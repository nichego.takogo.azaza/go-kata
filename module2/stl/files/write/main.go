package main

import (
	"bufio"
	"fmt"
	"os"
	"unicode"
	"unicode/utf8"
)

const fName = "example"

func main() {
	file, _ := os.Create(fName + ".txt")
	var s string
	fmt.Print("Строка для перевода в тринслит: ")
	s, _ = bufio.NewReader(os.Stdin).ReadString('\n')
	file.WriteString(s)
	file.Close()
	file, _ = os.Open(fName + ".txt")
	b := make([]byte, len(s))
	file.Read(b)
	file.Close()
	translit := map[string]string{"Аа": "Aa", "Бб": "Bb", "Вв": "Vv", "Гг": "Gg", "Дд": "Dd", "Ее": "Ee", "Ёё": "Yoyo", "Жж": "Zhzh", "Зз": "Zz", "Ии": "Ii", "Йй": "Yy", "Кк": "Kk", "Лл": "Ll", "Мм": "Mm", "Нн": "Nn", "Оо": "Oo", "Пп": "Pp", "Рр": "Rr", "Сс": "Ss", "Тт": "Tt", "Уу": "Uu", "Фф": "Ff", "Хх": "Hh", "Цц": "Tsts", "Чч": "Chch", "Шш": "Shsh", "Щщ": "Shchshch", "Ъъ": "\"\"", "Ыы": "Yy", "Ьь": "''", "Ээ": "Ee", "Юю": "Yuyu", "Яя": "Yaya"}
	for tdata, size, r := "", 0, ' '; utf8.RuneCount(b) > 0; {
		r, size = utf8.DecodeRune(b)
		b = b[size:]
		if unicode.Is(unicode.Cyrillic, r) {
			if v, ok := translit[string(unicode.ToUpper(r))+string(unicode.ToLower(r))]; ok {
				if unicode.IsUpper(r) {
					tdata += v[:len(v)/2]
				} else {
					tdata += v[len(v)/2:]
				}
			}
		} else {
			tdata += string(r)
		}
		if len(b) <= 0 {
			file, _ := os.Create(fName + ".processed.txt")
			file.WriteString(tdata)
			file.Close()
		}
	}
}
