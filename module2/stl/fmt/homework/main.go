package main

import "fmt"

func main() {
	generateSelfStory("Anton", 22, 450)
}

func generateSelfStory(name string, age int, money float64) {
	fmt.Printf("Hello! My name is %s, im %d (%b) y. o. And $%.2f is my year salary!!!", name, age, age, money)
}
