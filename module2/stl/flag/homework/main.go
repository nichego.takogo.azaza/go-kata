package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"os"
)

type Struct_ struct {
	Int2_    int
	String2_ string
}

type forMarshal struct {
	Int_      int
	String_   string
	Bool_     bool
	Array_    []interface{}
	Structure Struct_
}
type Config struct {
	Count   int           `json:"Int_"`
	Text    string        `json:"String_"`
	IsRight bool          `json:"Bool_"`
	Slc     []interface{} `json:"Array_"`
	Str     Struct_       `json:"Structure"`
}

func main() {
	test := forMarshal{ //Маршалинг, создание файла json
		Int_:    5,
		String_: "text1",
		Bool_:   false,
		Array_:  []interface{}{2, "tt", true},
		Structure: Struct_{
			Int2_:    8,
			String2_: "text2",
		},
	}
	b, err := json.Marshal(test)
	if err != nil {
		fmt.Println("Cant Marshal:", err)
		os.Exit(1)
	}
	file, err := os.Create("config.json")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	file.Write(b)
	file.Close()                                                       //Конец маршалинга
	check := flag.Bool("conf", false, "We've got configuration file!") // Начало задания
	flag.Parse()
	var filename string
	if *check {
		filename = flag.Arg(0)
	}
	file, err = os.Open(filename)
	if err != nil {
		fmt.Println("Cant open file:", filename)
		os.Exit(1)
	}
	defer file.Close()
	t := make([]byte, 5)
	text := make([]byte, 0, 40)
	for {
		n, err := file.Read(t)
		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Println("Cant read it:", err)
				os.Exit(1)
			}
		}
		text = append(text, t[:n]...)
	}
	if !json.Valid(text) {
		fmt.Println("JSON not valid")
		os.Exit(1)
	}
	file.Close()
	file, _ = os.Open(filename)
	str := Config{}
	json.NewDecoder(file).Decode(&str)
	fmt.Printf("Config: %+v", str)
}
