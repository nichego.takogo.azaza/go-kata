package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	buf := new(bytes.Buffer) // здесь расположите буфер
	for _, s := range data { // запишите данные в буфер
		n, err := buf.Write([]byte(s))
		if err != nil || n < len(s) {
			fmt.Println("ERROR! Cant write to buffer")
			os.Exit(1)
		}
		buf.Write([]byte("\n"))
	}
	file, _ := os.Create("example.txt") // создайте файл
	io.Copy(file, buf)                  // запишите данные в файл
	buf2 := new(bytes.Buffer)
	p := make([]byte, 5)
	file.Close()
	file, _ = os.Open("example.txt")
	defer file.Close()
	for { // прочтите данные в новый буфер
		n, err := file.Read(p)
		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Println(err)
				os.Exit(1)
			}
		}
		buf2.Write(p[:n])
	}

	fmt.Println(buf2.String()) // выведите данные из буфера buffer.String()
	// у вас все получится!
}
