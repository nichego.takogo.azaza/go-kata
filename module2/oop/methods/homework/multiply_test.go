package main

import "testing"

func TestMultiply(t *testing.T) {
	tests := [...]struct {
		a, b, want float64
	}{
		{0, 0, 0},
		{5, 7, 35},
		{10, -6, -60},
		{13.5, 2, 27},
		{3.7, 10, 37},
		{-4.5, 9, -40.5},
		{15, -4.7, -70.5},
		{-7.2, -2.5, 18},
	}
	for _, test := range tests {
		if got := multiply(test.a, test.b); got != test.want {
			t.Errorf("multiply(%.2f, %.2f) = %.2f, want %.2f", test.a, test.b, got, test.want)
		}
	}
}
