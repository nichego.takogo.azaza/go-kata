package main

import "testing"

func TestSum(t *testing.T) {
	tests := [...]struct {
		a, b, want float64
	}{
		{0, 0, 0},
		{5, 7, 12},
		{10, -6, 4},
		{13.5, 2.5, 16},
		{3.7, 3.7, 7.4},
		{-4.7, 1.7, -3},
		{100, -4.7, 95.3},
		{-7.2, -2.8, -10},
	}
	for _, test := range tests {
		if got := sum(test.a, test.b); got != test.want {
			t.Errorf("sum(%.2f, %.2f) = %.2f, want %.2f", test.a, test.b, got, test.want)
		}
	}
}
