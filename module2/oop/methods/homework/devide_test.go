package main

import "testing"

func TestDevide(t *testing.T) {
	tests := [...]struct {
		a, b, want float64
	}{
		{0, 1, 0},
		{5, 10, 0.5},
		{9, -6, -1.5},
		{12.5, 2.5, 5},
		{3.7, 3.7, 1},
		{-4.9, 2.45, -2},
		{100, -4, -25},
		{-6.8, -1.7, 4},
	}
	for _, test := range tests {
		if got := devide(test.a, test.b); got != test.want {
			t.Errorf("devide(%.2f, %.2f) = %.2f, want %.2f", test.a, test.b, got, test.want)
		}
	}
}
