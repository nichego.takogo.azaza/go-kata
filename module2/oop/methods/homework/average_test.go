package main

import "testing"

func TestAverage(t *testing.T) {
	tests := [...]struct {
		a, b, want float64
	}{
		{0, 0, 0},
		{5, 7, 6},
		{10, -6, 2},
		{13.5, 2.5, 8},
		{3.7, 3.7, 3.7},
		{-4.7, 1.7, -1.5},
		{100, -4.2, 47.9},
		{-7.2, -2.8, -5},
	}
	for _, test := range tests {
		if got := average(test.a, test.b); got != test.want {
			t.Errorf("average(%.2f, %.2f) = %.2f, want %.2f", test.a, test.b, got, test.want)
		}
	}
}
