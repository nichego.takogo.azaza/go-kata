package bench

import (
	"testing"
)

// MAP
func SelectXIntMap(x int, b *testing.B) {
	testm := make(map[int]int, x)
	for i := 0; i < x; i++ {
		testm[i] = i
	}
	var handle int
	_ = handle
	b.ResetTimer()
	for i := 0; i < x; i++ {
		handle = testm[i]
	}
}
func BenchmarkSelectXIntMap1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SelectXIntMap(1_000_000, b)
	}
}
func BenchmarkSelectXIntMap100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SelectXIntMap(100_000, b)
	}
}
func BenchmarkSelectXIntMap10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SelectXIntMap(10_000, b)
	}
}
func BenchmarkSelectXIntMap1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SelectXIntMap(1000, b)
	}
}
func BenchmarkSelectXIntMap100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SelectXIntMap(100, b)
	}
}

// SLICE
func SelectXIntSlice(x int, b *testing.B) {
	testslc := make([]int, x)
	for i := 0; i < x; i++ {
		testslc[i] = i
	}
	var handle int
	_ = handle
	b.ResetTimer()
	for i := 0; i < x; i++ {
		handle = testslc[i]
	}
}
func BenchmarkSelectXIntSlice1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SelectXIntSlice(1_000_000, b)
	}
}
func BenchmarkSelectXIntSlice100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SelectXIntSlice(100_000, b)
	}
}
func BenchmarkSelectXIntSlice10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SelectXIntSlice(10_000, b)
	}
}
func BenchmarkSelectXIntSlice1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SelectXIntSlice(1000, b)
	}
}
func BenchmarkSelectXIntSlice100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SelectXIntSlice(100, b)
	}
}
