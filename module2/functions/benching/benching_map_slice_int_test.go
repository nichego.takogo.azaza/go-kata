package bench

import (
	"testing"
)

// MAP
func InsertXIntMap(x int, b *testing.B) {
	testm := make(map[int]int, 0)
	b.ResetTimer()
	for i := 0; i < x; i++ {
		testm[i] = i
	}
}

func BenchmarkInsertIntMap1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntMap(1_000_000, b)
	}
}
func BenchmarkInsertIntMap100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntMap(100_000, b)
	}
}
func BenchmarkInsertIntMap10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntMap(10_000, b)
	}
}
func BenchmarkInsertIntMap1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntMap(1000, b)
	}
}
func BenchmarkInsertIntMap100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntMap(100, b)
	}
}

// SLICE
func InsertXIntSlice(x int, b *testing.B) {
	testslc := make([]int, 0)
	b.ResetTimer()
	for i := 0; i < x; i++ {
		testslc = append(testslc, i)
	}
}

func BenchmarkInsertIntSlice1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntSlice(1_000_000, b)
	}
}
func BenchmarkInsertIntSlice100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntSlice(100_000, b)
	}
}
func BenchmarkInsertIntSlice10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntSlice(10_000, b)
	}
}
func BenchmarkInsertIntSlice1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntSlice(1000, b)
	}
}
func BenchmarkInsertIntSlice100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntSlice(100, b)
	}
}
