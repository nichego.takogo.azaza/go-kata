package bench

import (
	"testing"
)

func InsertXInterfaceMap(x int, b *testing.B) {
	testm := make(map[interface{}]int, 0)
	b.ResetTimer()
	for i := 0; i < x; i++ {
		testm[i] = i
	}
}

func BenchmarkInsertInterfaceMap1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXInterfaceMap(1_000_000, b)
	}
}
func BenchmarkInsertInterfaceMap100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXInterfaceMap(100_000, b)
	}
}
func BenchmarkInsertInterfaceMap10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXInterfaceMap(10_000, b)
	}
}
func BenchmarkInsertInterfaceMap1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXInterfaceMap(1000, b)
	}
}
func BenchmarkInsertInterfaceMap100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXInterfaceMap(100, b)
	}
}
