package bench

import (
	"testing"
)

// MAP
func InsertXIntMapPre(x int, b *testing.B) {
	testm := make(map[int]int, x)
	b.ResetTimer()
	for i := 0; i < x; i++ {
		testm[i] = i
	}
}

func BenchmarkInsertIntMapPre1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntMapPre(1_000_000, b)
	}
}
func BenchmarkInsertIntMapPre100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntMapPre(100_000, b)
	}
}
func BenchmarkInsertIntMapPre10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntMapPre(10_000, b)
	}
}
func BenchmarkInsertIntMapPre1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntMapPre(1000, b)
	}
}
func BenchmarkInsertIntMapPre100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntMapPre(100, b)
	}
}

// SLICE
func InsertXIntSlicePre(x int, b *testing.B) {
	testslc := make([]int, x)
	b.ResetTimer()
	for i := 0; i < x; i++ {
		testslc[i] = i
	}
}

func BenchmarkInsertIntSlicePre1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntSlicePre(1_000_000, b)
	}
}
func BenchmarkInsertIntSlicePre100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntSlicePre(100_000, b)
	}
}
func BenchmarkInsertIntSlicePre10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntSlicePre(10_000, b)
	}
}
func BenchmarkInsertIntSlicePre1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntSlicePre(1000, b)
	}
}
func BenchmarkInsertIntSlicePre100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		InsertXIntSlicePre(100, b)
	}
}
