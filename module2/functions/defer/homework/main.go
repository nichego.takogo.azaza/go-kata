package main

import (
	"errors"
	"fmt"
)

type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	isFinished bool
}

func main() {
	a := MergeDictsJob{
		[]map[string]string{{"a0": "a0", "a1": "a1", "a2": "a2"}, {"b0": "b0", "b1": "b1", "b2": "b2"}, {"a0": "AAA", "b1": "BBB", "c0": "c0"}},
		map[string]string{},
		false,
	}
	err := ExecuteMergeDictsJob(&a)
	fmt.Println(a.Merged)
	if err != nil {
		fmt.Print(err)
		return
	}
	fmt.Print("Succes!")
}

func ExecuteMergeDictsJob(job *MergeDictsJob) error {
	defer func() { job.isFinished = true }()
	if len(job.Dicts) < 2 {
		return errors.New("at least 2 dictionaries are required")
	}
	for _, m := range job.Dicts {
		if m == nil {
			return errors.New("nil dictionary")
		}
		for i, v := range m {
			if _, ok := job.Merged[i]; !ok {
				job.Merged[i] = v
			}
		}
	}
	return nil
}
