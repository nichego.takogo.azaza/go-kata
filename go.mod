module gitlab.com/nichego.takogo.azaza/go-kata

go 1.19

require (
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/brianvoe/gofakeit/v6 v6.21.0
	github.com/go-chi/chi/v5 v5.0.8
	github.com/json-iterator/go v1.1.12
	github.com/sirupsen/logrus v1.9.3
	github.com/tebeka/selenium v0.9.9
	gitlab.com/nichego.takogo.azaza/greet v0.0.0-20230329202620-6a7a86aa2a40
	golang.org/x/sync v0.2.0
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
	golang.org/x/net v0.7.0 // indirect
	golang.org/x/sys v0.9.0 // indirect
)
